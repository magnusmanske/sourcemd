#!/usr//bin/php
<?PHP

//ini_set('memory_limit','2500M');
//set_time_limit ( 60 * 10 ) ; // Seconds

require_once ( 'public_html/php/common.php' ) ;
require_once ( 'public_html/php/wikidata.php' ) ;

function isScientist ( $q ) {
	global $j_scientists ;
	return in_array ( $q , $j_scientists ) ;
}

$out = array ( 'status' => 'OK' , 'data' => array() ) ;

$sparql = 'SELECT ?q { ?q wdt:P31 wd:Q5 ; wdt:P106/wdt:P279* wd:Q901 }' ;
$j_scientists = getSPARQLitems ( $sparql ) ;
#$url = "http://wdq.wmflabs.org/api?q=claim[31:5]%20and%20claim[106:%28tree[901][][279]%29]" ; // All scientists
#$j_scientists = json_decode ( file_get_contents ( $url ) ) ;

#$url = "http://wdq.wmflabs.org/api?q=claim[2093]" ; // All short name items
#$j_short_names = json_decode ( file_get_contents ( $url ) ) ;

$r = rand()/getrandmax(); # 0<=$r<1
$batch_size = 5000 ;

$j_short_names = [] ;
$sql = 'SELECT page_title FROM page,pagelinks WHERE page_id=pl_from AND pl_namespace=120 AND pl_title="P2093" AND pl_from_namespace=0' ;
$sql .= " AND page_random>=$r ORDER BY page_random LIMIT $batch_size" ; # Random batch
$dbwd = openDB ( 'wikidata' , 'wikidata' , true ) ;
$result = getSQL ( $dbwd , $sql ) ;
while($o = $result->fetch_object()) $j_short_names[] = preg_replace ( '/\D/' , '' , $o->page_title ) ;

$wil = new WikidataItemList ;
$wil->loadItems ( $j_short_names ) ;

$db = openToolDB ( 'sourcemd_game_p' ) ;

foreach ( $j_short_names AS $qnum ) {
	$i = $wil->getItem ( $qnum ) ;
	if ( !isset($i) ) continue ; // Paranoia
	if ( !$i->hasClaims('P2093') ) continue ;
	$claims = $i->getClaims('P2093') ;
	foreach ( $claims AS $c ) {
		$statement_id = $c->id ;
		
		if ( !isset($c->mainsnak) ) continue ;
		if ( !isset($c->mainsnak->datavalue) ) continue ;
		if ( !isset($c->mainsnak->datavalue->value) ) continue ;
		$shortname = $c->mainsnak->datavalue->value ;
		
		if ( !isset($c->qualifiers) ) continue ;
		if ( !isset($c->qualifiers->P1545) ) continue ;
		if ( !isset($c->qualifiers->P1545[0]) ) continue ;
		if ( !isset($c->qualifiers->P1545[0]->datavalue) ) continue ;
		if ( !isset($c->qualifiers->P1545[0]->datavalue->value) ) continue ;
		$serialnum = $c->qualifiers->P1545[0]->datavalue->value * 1 ;
		
		$doi = $i->getFirstString ( 'P356' ) ;
		$pmid = $i->getFirstString ( 'P698' ) ;
		$pmcid = $i->getFirstString ( 'P932' ) ;
		$url = '' ;
		if ( $pmcid != '' ) $url = "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC$pmcid" ;
		else if ( $doi != '' ) $url = "https://dx.doi.org/" . urlencode($doi) ;
		else if ( $pmid != '' ) $url = "https://www.ncbi.nlm.nih.gov/pubmed/?term=$pmid" ;
		
		$sql = "SELECT DISTINCT wbit_item_id 
			FROM wbt_item_terms,wbt_term_in_lang,wbt_text_in_lang,wbt_text
			WHERE wbtl_id=wbit_term_in_lang_id
			AND wbtl_text_in_lang_id=wbxl_id
			AND wbxl_text_id=wbx_id
			AND wbtl_type_id IN (1,3)
			AND wbx_text='".$db->real_escape_string($shortname)."'" ;
		$result = getSQL ( $dbwd , $sql ) ;
		while($o = $result->fetch_object()){
			$q_author = $o->wbit_item_id ;
			if ( !isScientist($q_author) ) continue ;
			$sql = "INSERT IGNORE INTO candidates (q_publication,q_author,shortname,serialnum,statement_id,url,random) VALUES ($qnum,$q_author,'".$db->real_escape_string($shortname)."',$serialnum,'$statement_id','".$db->real_escape_string($url)."',rand())" ;
			getSQL ( $db , $sql ) ;
		}
	}
}

/*
$authors = json_decode ( get_request ( 'authors' , '[]' ) ) ;
$a2 = array();
foreach ( $authors AS $k => $v ) {
	$a2[] = $db->real_escape_string ( $v ) ;
}

$sql = "SELECT DISTINCT term_entity_id,term_text FROM wb_terms WHERE term_entity_type='item' AND term_type IN ('label','alias') AND term_text IN ('" . implode("','",$a2) . "')" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	if ( !in_array ( $o->term_entity_id , $j->items ) ) continue ;
	$out['data'][$o->term_text][] = $o->term_entity_id ;
}



//print json_encode ( $out ) ;
*/

?>