#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( '/data/project/sourcemd/scripts/orcid_shared.php' ) ;

$batch_size = 1 ;

function getQS () {
	$toolname = '' ; // Or fill this in manually
	$path = realpath(dirname(__FILE__)) ;
	$user = get_current_user() ;
	if ( $toolname != '' ) {}
	else if ( preg_match ( '/^tools\.(.+)$/' , $user , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/data\/project\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/mnt\/nfs\/[^\/]+\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	if ( $toolname == '' ) die ( "getQS(): Can't determine the toolname for $path\n" ) ;
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/$toolname/bot.ini" ;
	$qs->toolname = 'SourceMD:Fixing_P50_P2093' ;
	$qs->sleep = 1 ;
	return $qs ;
}


$sparql = "SELECT ?paper_q ?series_id ?author_q ?author_name {
  ?paper_q p:P50 ?author_statement .
  ?author_statement ps:P50 ?author_q .
  ?author_statement pq:P1545 ?series_id .
  ?paper_q p:P2093 ?author_name_statement .
  ?author_name_statement ps:P2093 ?author_name .
  ?author_name_statement pq:P1545 ?series_id
  } LIMIT $batch_size" ;
$j = getSPARQL ( $sparql ) ;

$commands = '' ;
foreach ( $j->results->bindings AS $x ) {
	if ( !preg_match ( '/^.+\/(Q\d+)$/' , $x->author_q->value , $m ) ) continue ;
	$author_q = $m[1] ;
	if ( !preg_match ( '/^.+\/(Q\d+)$/' , $x->paper_q->value , $m ) ) continue ;
	$paper_q = $m[1] ;
	$series = $x->series_id->value ;
	$name = $x->author_name->value ;
	
	$commands .= "$paper_q\tP50\t$author_q\tP1932\t\"$name\"\n" ;
	$commands .= "-$paper_q\tP2093\t\"$name\"\n" ;
}

print $commands ; exit(0); # TESTING

$qs = getQS() ;
$tmp = $qs->importData ( $commands , 'v1' ) ;
$qs->runCommandArray ( $tmp['data']['commands'] ) ;

?>