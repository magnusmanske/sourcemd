#!/usr/bin/php
<?PHP

require_once ( '/data/project/sourcemd/scripts/orcid_shared.php' ) ;

function getQS () {
	$toolname = '' ; // Or fill this in manually
	$path = realpath(dirname(__FILE__)) ;
	$user = get_current_user() ;
	if ( $toolname != '' ) {}
	else if ( preg_match ( '/^tools\.(.+)$/' , $user , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/data\/project\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/mnt\/nfs\/[^\/]+\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	if ( $toolname == '' ) die ( "getQS(): Can't determine the toolname for $path\n" ) ;
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/$toolname/bot.ini" ;
	$qs->toolname = 'SourceMD:AddORCIDinfo' ;
	$qs->sleep = 1 ;
	return $qs ;
}

$wil = new WikidataItemList () ;

$sparql = "SELECT ?q { ?q wdt:P496 [] }" ;
$j = getSPARQLitems ( $sparql ) ;
$wil->loadItems ( $j ) ;
foreach ( $j AS $q ) {
	$res = add_person_ids_from_orcid ( "Q$q" ) ;
	if ( $res != 'OK' ) print "$res\n" ;
}

?>