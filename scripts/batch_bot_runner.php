#!/usr/bin/php
<?php

require_once ( '/data/project/sourcemd/scripts/batch_bot.php' ) ;

$debugging = false ;
$is_bot_mode = 1 ; # Suppress print from SourceMD class
$bb = new BatchBot () ;

# e.g. jsub -N bb_24 -mem 6g -once -quiet /data/project/sourcemd/scripts/batch_bot_runner.php 24
$specific_job = 0 ;
if ( isset($argv[1]) ) {
	if ( $argv[1] == 'auto' ) {
		$minutes = 10*60 ; # 10 minutes since last action
		$sql = "SELECT * FROM batch WHERE `status`='TODO' AND (now()-last_action)>{$minutes}" ;
		$sql .= ' AND EXISTS (SELECT * FROM command WHERE batch_id=batch.id AND `status` IN ("RUNNING","TODO") AND `mode` NOT IN ("CREATE_PAPER_BY_ID","ADD_AUTHOR_TO_PUBLICATION"))' ; // Only the ones not taken by the Rust bot
		$sql .= " ORDER BY last_action LIMIT 1" ;
		$result = $bb->tfc->getSQL ( $bb->dbt , $sql ) ;
		if($o = $result->fetch_object()) {
			$specific_job = $o->id ;
		} else exit(0) ; # All batches are spoken for
	} else {
		$specific_job = $argv[1] * 1 ;
	}
}

# Reset RUNNING tasks to TODO
if ( $specific_job == 0 ) {
	$sql = "UPDATE `command` SET `status`='TODO' WHERE `status`='RUNNING'" ;
	$bb->tfc->getSQL ( $bb->dbt , $sql ) ;
}

if ( $specific_job != 0 ) {
	$bb->updateBatchStatus ( $specific_job ) ;
	$sql = "UPDATE `command` SET `status`='TODO' WHERE `status`='RUNNING' AND batch_id={$specific_job}" ;
	$bb->tfc->getSQL ( $bb->dbt , $sql ) ;
}

$iteration = 0 ;
while ( 1 ) {
	$iteration++ ;
	if($debugging) print "ITERATION {$iteration}\n" ;

	if ( $iteration % 100 == 0 ) { # Memory save
		 $wil = new WikidataItemList () ;
		 $bb->wil = $wil ;
	}

	$cnt = 0 ;
	if ( $specific_job == 0 ) {
		$cnt = $bb->runNextCommands () ;
	} else {
		$cnt = $bb->runNextCommands ( $specific_job ) ;
	}
	if ( $cnt == 0 ) {
		if ( isset($specific_job) ) { # SPECIFIC JOB IS DONE
			print "Job $specific_job done!\n" ;
			exit(0) ;
		}
		sleep(10) ;
	}
}


?>