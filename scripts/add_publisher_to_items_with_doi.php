#!/usr/bin/php
<?PHP

/*
THIS REQUIRES A FLATFILE, CREATED THUS:
echo 'SELECT DISTINCT pl_from FROM pagelinks WHERE pl_namespace=0 AND pl_title IN ("Q591041","Q13442814") AND pl_from_namespace=0' | sql wikidata > papers.tab
echo 'SELECT DISTINCT pl_from FROM pagelinks WHERE pl_namespace=120 AND pl_title="P356" AND pl_from_namespace=0' | sql wikidata > with_doi.tab
echo 'SELECT DISTINCT pl_from FROM pagelinks WHERE pl_namespace=120 AND pl_title="P1433" AND pl_from_namespace=0' | sql wikidata > with_published_in.tab
cat papers.tab with_doi.tab | sort | uniq -d > papers_with_doi.tab
cat papers_with_doi.tab with_published_in.tab | sort | uniq -u > only_pwd_or_published.tab
cat only_pwd_or_published.tab papers_with_doi.tab | sort | uniq -d > papers_with_doi_but_not_published.tab
rm papers.tab with_doi.tab with_published_in.tab papers_with_doi.tab only_pwd_or_published.tab
*/

require_once ( '/data/project/sourcemd/scripts/orcid_shared.php' ) ;

$create_new_authors = true ;

function getQS () {
	$toolname = '' ; // Or fill this in manually
	$path = realpath(dirname(__FILE__)) ;
	$user = get_current_user() ;
	if ( $toolname != '' ) {}
	else if ( preg_match ( '/^tools\.(.+)$/' , $user , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/data\/project\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/mnt\/nfs\/[^\/]+\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	if ( $toolname == '' ) die ( "getQS(): Can't determine the toolname for $path\n" ) ;
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/$toolname/bot.ini" ;
	$qs->toolname = 'SourceMD:AddPublisherViaDoi' ;
	$qs->sleep = 5 ;
	return $qs ;
}

function runCommands ( $commands ) {
	$qs = getQS() ;
	$qs->use_command_compression = true ;
	$tmp = $qs->importData ( implode ( "\n" , $commands ) , 'v1' ) ;
	$tmp['data']['commands'] = $qs->compressCommands ( $tmp['data']['commands'] ) ;
	$qs->runCommandArray ( $tmp['data']['commands'] ) ;
	return $qs->last_item ;
}

$issn_cache = [] ;
function getOrCreateJournalFromISSN ( $issn , $title = '' ) {
	global $issn_cache ;
	if ( $title == '' ) $title = "ISSN {$issn}" ;
	$commands = [ "CREATE" ] ;
	$commands[] = "LAST\tP31\tQ5633421" ;
	$commands[] = "LAST\tLen\t\"$title\"" ;
	$commands[] = "LAST\tP236\t\"$issn\"" ;
	$ret = runCommands ( $commands ) ;
	$issn_cache[strtolower($issn)] = $ret ;
	return $ret ;
}


function addJournalToPaper ( $paper_q ) {
	global $issn_cache ;
	$wil = new WikidataItemList ;
	$wil->loadItems ( [$paper_q] ) ;
	$i = $wil->getItem ( $paper_q ) ;
	if ( !isset($i) ) return ;
	if ( !$i->hasClaims('P356') ) return ;
	if ( $i->hasClaims('P1433') ) return ;
	$doi = $i->getFirstString ( 'P356' ) ;
	print "$doi\thttps://www.wikidata.org/wiki/{$paper_q}\n" ;
return;
	$smd = new SourceMD ( $doi , false ) ;
	$smd->loadDOI () ;
	$commands = [] ;
	if ( isset($smd->data['P1433']) ) {
		$journal_q = $smd->data['P1433'][0] ;
		$commands[] = "{$paper_q}\tP1433\t{$journal_q}" ;
		print "ADDING TO EXISTING JOURNAL: https://www.wikidata.org/wiki/{$journal_q}\n" ;
	} else if ( count($smd->issn) > 0 ) {
		$journal_q = '' ;
		foreach ( $smd->issn AS $issn => $title ) {
			if ( isset($issn_cache[strtolower($issn)]) ) {
				$journal_q = $issn_cache[strtolower($issn)] ;
				print "ADDING TO EXISTING JOURNAL (RECENTLY CREATED): https://www.wikidata.org/wiki/{$journal_q}\n" ;
				break ;
			}
			$journal_q = getOrCreateJournalFromISSN ( $issn , $title ) ;
			print "CREATED NEW JOURNAL: https://www.wikidata.org/wiki/{$journal_q}\n" ;
			if ( isset($journal_q) and $journal_q != '' ) break ;
		}
		if ( isset($journal_q) and $journal_q != '' ) {
			$commands[] = "{$paper_q}\tP1433\t{$journal_q}" ;
		}
	} else if ( count($smd->isbn) > 0 ) {
		foreach ( $smd->isbn AS $isbn => $title ) {
			$book_q = getOrCreateBookFromISBN ( $isbn , $title ) ;
			if ( !isset($book_q ) ) continue ;
			$commands[] = "{$paper_q}\tP1433\t{$book_q}" ;
			print "ADDING: https://www.wikidata.org/wiki/{$paper_q} published in https://www.wikidata.org/wiki/{$book_q}\n" ;
			break ;
		}
	}
	if ( count($commands) == 0 ) return ;
	runCommands ( $commands ) ;
}

$tfc = new ToolforgeCommon ;

$rows = explode ( "\n" , trim ( file_get_contents ( 'papers_with_doi_but_not_published.tab') ) ) ;
$db = $tfc->openDB ( 'wikidata' , 'wikidata' ) ;

$sql = "SELECT * FROM pagelinks pl1 WHERE pl_title='P1433' AND pl_namespace=120 AND pl_from IN (" . implode(',',$rows) . ")" ;
$result = $tfc->getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()){
	if(($key = array_search($o->pl_from, $rows)) !== false) unset($rows[$key]);
}

foreach ( $rows AS $page_id ) {
	$page_id = trim ( $page_id ) ;
	if ( $page_id == '' ) continue ;
	$sql = "SELECT * FROM page WHERE page_id={$page_id}" ;
	$result = $tfc->getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()){
		if ( $o->page_namespace != 0 ) continue ; // Paranoia
		$paper_q = $o->page_title ;
		//print "https://www.wikidata.org/wiki/$paper_q\n" ;
		addJournalToPaper ( $paper_q ) ;
	}
}


?>