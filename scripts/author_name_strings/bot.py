#!/usr/bin/env python3

"""
This script takes a paper ("scholarly article"/Q13442814) Wikidata item,
and uses the P50s to find all other papers co-authored by at least two of
the original P50s. It then groups the "authos name strings"/P2093 and tries
to replace them with P50.
First, it tries to find if any of the P50 authors have a matching name to a P2093.
Then, it looks for P2093 with (at least) "full-length" name parts, eg. "John Smith"
(from eg "John X Y Z Smith").
If there is exactly one search results for people with that name, and that item
happens to be a researcher, P2093 is replaced with P50.
If there is no search result, a new author item is created, and P2093 is replaced with P50.
"""

import re
import sys
import signal
import toolforge
import configparser
from threading import Thread
from wikibaseintegrator import WikibaseIntegrator
from wikibaseintegrator.wbi_config import config as wbi_config
from wikibaseintegrator import wbi_login
from wikibaseintegrator import wbi_helpers
from wikibaseintegrator.entities.item import ItemEntity
from wikibaseintegrator.models.references import Reference, References
from wikibaseintegrator.models.qualifiers import Qualifiers
from wikibaseintegrator.models.labels import Labels
from wikibaseintegrator.models.aliases import Aliases
from wikibaseintegrator.models.descriptions import Descriptions
from wikibaseintegrator.models.claims import Claims
from wikibaseintegrator.datatypes.item import Item
from wikibaseintegrator.datatypes.string import String

class AuthorNameStringsTimeoutException(Exception):
    pass

class AuthorNameStrings:
	def __init__(self, login=True):
		self.wbi = self.initialize(login)
		self.git_url = 'https://bitbucket.org/magnusmanske/sourcemd/src/master/scripts/author_name_strings/bot.py'
		self.summary_string = f"SourceMD author name string resolver script {self.git_url}"
		self.organization_pattern = r"\b(study|group|society|initiative|the|consortium|analysis|faculty)\b"
		self.min_P2093_occurrences = 2
		self.loading_chunk_size = 50
		self.verbose = True
		self.write_timeout = 60*3 # seconds
		self.clear()

	def timeout_handler(self,signum, frame):
		raise AuthorNameStringsTimeoutException

	def clear(self):
		self.item_cache = {}
		self.created_authors = {}
		self.p2093_to_p50_changed = 0
		self.new_author_items_created = 0

	def initialize(self,login=False):
		"""Returns a WikibaseIntegrator instance, oprionally logged in as bot."""
		wbi_config['USER_AGENT'] = 'SourceMD/1.0 (https://www.wikidata.org/wiki/User:Magnus_Manske)'
		if login:
			config = configparser.ConfigParser()
			config.read('/data/project/sourcemd/bot.ini')
			login_instance = wbi_login.Login(user=config['user']['user'], password=config['user']['pass'])
			return WikibaseIntegrator(login=login_instance, is_bot=True)
		else:
			return WikibaseIntegrator()

	def get_paper_items_for_author_item_ids(self,author_item_ids):
		"""Get potential paper items with at least two of the known authors, and existing author name strings."""
		query = 'SELECT page_title,count(*) AS p50_count FROM page,pagelinks WHERE pl_title IN ("' + '","'.join(author_item_ids) + '") AND pl_namespace=0 AND pl_from_namespace=0 '
		query += """AND page_id=pl_from
		AND EXISTS (SELECT * FROM pagelinks p2 WHERE pl_from=page_id AND pl_title='P2093' AND pl_namespace=120) # has author name string(s)
		AND EXISTS (SELECT * FROM pagelinks p2 WHERE pl_from=page_id AND pl_title='Q13442814' AND pl_namespace=0) # has "scholarly work"
		GROUP BY pl_from HAVING p50_count>=2 ORDER BY p50_count DESC,page_title"""
		with toolforge.connect('wikidatawiki_p').cursor() as cur:
			cur.execute(query)
			output = list(map(lambda row: row[0].decode('utf-8'),cur.fetchall()))
		return output

	def get_entity(self,entity_id):
		"""Loads a single Wikidata entity into cache, and returns it."""
		if entity_id not in self.item_cache:
			self.item_cache[entity_id] = self.wbi.item.get(entity_id=entity_id)
		return self.item_cache[entity_id]

	def load_entities_chunk(self,entity_ids):
		data = {"action":"wbgetentities","ids":"|".join(entity_ids)}
		json_data = wbi_helpers.mediawiki_api_call_helper(data=data, allow_anonymous=True)
		for entity_id in json_data['entities'].keys():
			self.item_cache[entity_id] = ItemEntity(api=self.wbi).from_json(json_data=json_data['entities'][entity_id])

	def load_entities(self,entity_ids):
		"""Loads a larger number of Wikidata entities. Takes care not to load the same one twice."""
		entity_ids = list(set(entity_ids)) # Unique
		entity_ids = list(filter(lambda entity_id: entity_id not in self.item_cache, entity_ids)) # Only the ones not loaded
		entity_ids = list(self.chunk_list(entity_ids,self.loading_chunk_size))
		threads = []
		max_concurrent = 10
		while len(entity_ids)>0 or len(threads)>0:
			while len(entity_ids)>0 and len(threads)<max_concurrent:
				entity_ids_chunk = entity_ids.pop()
				t = Thread(target=self.load_entities_chunk, args=(entity_ids_chunk, ))
				t.start()
				threads.append(t)
			if len(threads)>0:
				t = threads.pop()
				t.join()

	def chunk_list(self,list_to_chunk, max_chunk_size):
		for i in range(0, len(list_to_chunk), max_chunk_size):
			yield list_to_chunk[i:i + max_chunk_size]

	def has_claims(self,item,prop):
		return prop in item.claims

	def get_claims(self,item,prop):
		if self.has_claims(item,prop):
			return item.claims.get(prop)
		else:
			return []

	def extract_ans(self,item_ids):
		simple_name_groups = {}
		for item_id in item_ids:
			paper_item = self.get_entity(item_id)
			for snak in self.get_claims(paper_item,'P2093'):
				ans = snak.mainsnak.datavalue['value']
				simple_ans = self.simplify_name(ans)
				if simple_ans not in simple_name_groups:
					simple_name_groups[simple_ans] = { 'simple_name':simple_ans, 'occurrences':[] }
				simple_name_groups[simple_ans]['occurrences'].append({'paper_id':item_id,'name':ans,'snak':snak})
		return simple_name_groups

	def extract_author_item_ids(self,paper_items_ids):
		author_item_ids = []
		for paper_items_id in paper_items_ids:
			paper_item = self.get_entity(paper_items_id)
			for snak in self.get_claims(paper_item,'P50'):
				author_item_id = snak.mainsnak.datavalue['value']['id']
				author_item_ids.append(author_item_id)
		return list(set(author_item_ids))

	def is_valid_simple_name(self,simple_name):
		if len(simple_name)<=4:
			return False
		return True

	def detect_known_authors(self,author_item_ids,name_groups):
		"""In the found papers, detects ANS that should be """
		known_author2item_id = {}
		for author_item_id in author_item_ids:
			author_item = self.get_entity(author_item_id)
			for name in author_item.aliases.get():
				name = str(name)
				simple_name = self.simplify_name(name)
				if not self.is_valid_simple_name(simple_name):
					continue
				if simple_name in known_author2item_id and author_item_id!=known_author2item_id[simple_name]:
					if self.verbose:
						print(f"WARNING: Collison for {simple_name} ({author_item_id}/{known_author2item_id[simple_name]})")
					continue
				known_author2item_id[simple_name] = author_item_id
		
		for ans in name_groups.keys():
			if ans not in known_author2item_id.keys():
				continue
			author_item_id = known_author2item_id[ans]
			author_item = self.get_entity(author_item_id)
			author_name = author_item.labels.get("en") or author_item_id
			for occurrence in name_groups[ans]['occurrences']:
				paper_item_id = occurrence['paper_id']
				paper_item = self.get_entity(paper_item_id)
				author_item_ids = list(map(lambda snak: snak.mainsnak.datavalue['value']['id'] ,self.get_claims(paper_item,'P50')))
				if author_item_id in author_item_ids:
					continue
				self.change_p2093_to_p50(occurrence,author_item_id)
			name_groups[ans]['occurrences'] = list(filter(lambda occurrence: 'removed' not in occurrence, name_groups[ans]['occurrences']))

	def change_p2093_to_p50(self,occurrence,author_item_id):
		snak = occurrence['snak']
		qualifiers_json = snak.qualifiers.get_json()
		if 'P1932' not in qualifiers_json:
			qualifiers_json['P1932'] = [{'snaktype': 'value', 'property': 'P1932', 'datatype': 'string', 'datavalue': {'value': occurrence['name'], 'type': 'string'}}]

		p50 = Item(value=author_item_id,prop_nr=50)
		p50.qualifiers.from_json(qualifiers_json)
		p50.references = snak.references
		paper_item_id = occurrence['paper_id']
		paper_item = self.get_entity(paper_item_id)

		# Check what would be removed
		removed_counter = 0
		for claim in self.get_claims(paper_item,'P2093'):
			if claim.mainsnak.datavalue['value']==snak.mainsnak.datavalue['value']:
				removed_counter += 1
		if removed_counter>1:
			if self.verbose:
				print(f"ERROR: Removed '{snak.mainsnak.datavalue['value']}'{removed_counter}x P2093 from {paper_item_id}. O50 not added, P2093 not removed.")
			return
		if removed_counter==0:
			if self.verbose:
				print(f"WARNING: Did not remove any P2093 from {paper_item_id}. P50 not added.")
			return

		# Remove P2093 and add p50
		for claim in self.get_claims(paper_item,'P2093'):
			if claim.mainsnak.datavalue['value']==snak.mainsnak.datavalue['value']:
				claim.remove()
		paper_item.add_claims(p50)
		self.entities_to_update[paper_item_id] = True
		occurrence['removed'] = True
		self.p2093_to_p50_changed += 1

	def create_new_author(self,author_details):
		# Find best name
		best_name = '' ;
		an = {}
		for occurrence in author_details['occurrences']:
			ans = occurrence['name']
			if ans in an:
				an[ans] += 1
			else:
				an[ans] = 1
		an_keys = sorted(an.keys(), key=lambda x: len(x)*-1000-an[x])

		# Create item
		labels = Labels().from_json({"en":{"language":"en", "value":an_keys[0]}})
		descriptions = Descriptions().from_json({"en":{"language":"en", "value":"researcher"}})
		aliases = Aliases()
		for alias in an_keys[1:]:
			aliases.set("en",alias)
		claims = Claims()
		if re.search(self.organization_pattern,an_keys[0].lower()):
			claims.add(Item(prop_nr=31, value="Q1241025"))
		else:
			claims.add(Item(prop_nr=31, value="Q5"))
			claims.add(Item(prop_nr=106, value="Q1650915"))
		new_author = ItemEntity(labels=labels,aliases=aliases,descriptions=descriptions,claims=claims,api=self.wbi)

		signal.signal(signal.SIGALRM, self.timeout_handler)
		signal.alarm(self.write_timeout)
		new_author = new_author.write(summary=self.summary_string)
		signal.alarm(0)
		#except AuthorNameStringsTimeoutException:

		self.item_cache[new_author.id] = new_author
		self.new_author_items_created += 1
		return new_author.id

	def has_item_links(self,item,prop,values):
		for claim in self.get_claims(item,prop):
			if claim.mainsnak.datavalue['value']['id'] in values:
				return True
		return False

	def match_or_create_new_author(self,group):
		max_results = 20
		valid_p31 = ['Q1650915','Q39631']

		simple_name = group['simple_name']
		if len(group['occurrences'])<self.min_P2093_occurrences: # Not enough occurrences
			return
		if simple_name.find(' ')<0: # Insist on at least two name parts
			return

		# Get potential author items from search and recently created authors (might not be in search index)
		data = {"action":"query","list":"search","srsearch":f"{simple_name} haswbstatement:P31=Q5","srlimit":max_results,"srnamespace":"0"}
		json_data = wbi_helpers.mediawiki_api_call_helper(data=data, allow_anonymous=True)
		potential_author_item_ids = list(map(lambda entry: entry['title'], json_data['query']['search']))
		if simple_name in self.created_authors:
			potential_author_item_ids.append(self.created_authors[simple_name])
			potential_author_item_ids = list(set(potential_author_item_ids))

		if len(potential_author_item_ids)>=max_results: # Too many results
			return
		if len(potential_author_item_ids)==0:
			author_item_id = self.create_new_author(group)
			self.created_authors[simple_name] = author_item_id
			if self.verbose:
				print(f"Created '{simple_name}' as {author_item_id}")
			for occurrence in group['occurrences']:
				self.change_p2093_to_p50(occurrence,author_item_id)
			return
		self.load_entities(potential_author_item_ids)
		researcher_item_ids = list(filter(lambda author_item_id: self.has_item_links(self.get_entity(author_item_id),'P106',valid_p31), potential_author_item_ids))
		if len(researcher_item_ids)==1:
			author_item_id = researcher_item_ids[0]
			if self.verbose:
				print(f"Changing '{simple_name}' to {author_item_id}")
			for occurrence in group['occurrences']:
				self.change_p2093_to_p50(occurrence,author_item_id)
		else:
			if self.verbose:
				print(f"Don't know what to do with '{simple_name}' ({len(potential_author_item_ids)} matches)")

		# Cleanup
		for author_id in potential_author_item_ids:
			del self.item_cache[author_id]

	def match_or_create_new_authors(self,name_groups):
		for group in name_groups.values():
			try:
				self.match_or_create_new_author(group)
			except:
				if self.verbose:
					print(f"FAILED TO MATCH/CREATE AUTHOR {group['simple_name']}")

	def simplify_name(self,ans):
		simple_ans = ans
		simple_ans = re.sub(r'\S+\.', ' ', simple_ans)
		simple_ans = re.sub(r'\b\S{1,2}\b', ' ', simple_ans, re.UNICODE)
		simple_ans = re.sub(r'\s+', ' ', simple_ans)
		return simple_ans.strip()

	def process_paper(self,paper_item_id):
		paper_item = self.get_entity(paper_item_id)
		author_item_ids = list(map(lambda claim: claim.mainsnak.datavalue['value']['id'] ,self.get_claims(paper_item,'P50')))
		if not author_item_ids:
			raise Exception(f"No P50 found in {paper_item_id}")
		paper_items_ids = self.get_paper_items_for_author_item_ids(author_item_ids)
		if not paper_items_ids:
			raise Exception(f"No potential papers found for authors in {paper_item_id}")
		self.load_entities(paper_items_ids)
		name_groups = self.extract_ans(paper_items_ids)
		author_item_ids = self.extract_author_item_ids(paper_items_ids)
		self.load_entities(author_item_ids)
		self.entities_to_update = {}
		self.detect_known_authors(author_item_ids,name_groups)
		self.match_or_create_new_authors(name_groups)

		# Write changes
		for paper_item_id in self.entities_to_update.keys():
			try:
				if self.verbose:
					print(f"Writing to https://www.wikidata.org/wiki/{paper_item_id}")
				paper_item = self.get_entity(paper_item_id)
				signal.signal(signal.SIGALRM, self.timeout_handler)
				signal.alarm(self.write_timeout)
				self.item_cache[paper_item_id] = paper_item.write(summary=self.summary_string)
				signal.alarm(0)
			except:
				if self.verbose:
					print(f"FAILED TO UPDATE https://www.wikidata.org/wiki/{paper_item_id}")

		print(f"{self.new_author_items_created} New author items created")
		print(f"{self.p2093_to_p50_changed} P2093=>P50 statements changed")

		# Cleanup
		self.created_authors = {}

	def get_random_paper_item_ids(self):
		data = {"action":"query","list":"random","rnnamespace":0,"rnlimit":50}
		json_data = wbi_helpers.mediawiki_api_call_helper(data=data, allow_anonymous=True)
		item_ids = list(map(lambda x: x['title'],json_data['query']['random']))
		self.load_entities(item_ids)
		ret = []
		for item_id in item_ids:
			item = self.get_entity(item_id)
			if self.has_item_links(item,'P31',['Q13442814']) and len(self.get_claims(item,'P50'))>=2:
				ret.append(item_id)
			else:
				del self.item_cache[item_id]
		return ret

	def run_on_random_items(self):
		paper_item_ids = self.get_random_paper_item_ids()
		for paper_item_id in paper_item_ids:
			print(f"\n======\nChecking paper https://www.wikidata.org/wiki/{paper_item_id}")
			try:
				self.process_paper(paper_item_id)
			except:
				print("SOMETHING WENT WRONG")

	def fix_P1810(self):
		self.clear()
		summary = f"Replacing P1810 (subject named as) with P1932 (object named as) qualifier(s) for P50 (author) {self.git_url}"
		sparql = "SELECT ?q { ?q wdt:P31 wd:Q13442814 ; p:P50 ?statement . ?statement pq:P1810 ?name } LIMIT 100"
		result = wbi_helpers.execute_sparql_query(sparql)
		item_ids = list(map(lambda binding: binding['q']['value'].split("/")[-1],result['results']['bindings']))
		item_ids = list(set(item_ids))
		self.load_entities(item_ids)
		for item_id in item_ids:
			#if item_id in had_that:
			#	continue
			#had_that[item_id] = True
			if item_id not in self.item_cache:
				continue
			changed = False
			item = self.get_entity(item_id)
			claims = self.get_claims(item,'P50')
			for claim in claims:
				if 'P1810' not in claim.qualifiers.qualifiers:
					continue
				for qual in claim.qualifiers.qualifiers['P1810']:
					name = qual.datavalue['value']
					if name.isnumeric(): # Name is a number, suspicious
						continue
					claim.qualifiers.qualifiers['P1810'].remove(qual)
					new_qualifier = String(value=name,prop_nr=1932)
					claim.qualifiers.add(new_qualifier)
					changed = True
			if changed:
				if self.verbose:
					print(f"Writing to https://www.wikidata.org/wiki/{item_id}")
				signal.signal(signal.SIGALRM, self.timeout_handler)
				signal.alarm(self.write_timeout)
				item.write(summary=summary)
				signal.alarm(0)



if __name__ == "__main__" :
	if len(sys.argv)==1: # No specific paper, try random ones
		while True:
			ans_bot = AuthorNameStrings()
			ans_bot.verbose = False
			try:
				ans_bot.run_on_random_items()
			except:
				pass
			try:
				ans_bot.fix_P1810()
			except:
				pass

	elif len(sys.argv)==2: # Specific paper
		ans_bot = AuthorNameStrings()
		ans_bot.process_paper(sys.argv[1])

