#!/usr/bin/php
<?PHP

require_once ( '/data/project/sourcemd/scripts/science_source/science_source.php' ) ;
require_once ( '/data/project/sourcemd/scripts/orcid_shared.php' ) ;

if ( !isset($argv[1]) ) die ( "USAGE: [update_from_rc|update_from_pageid|update_pubs_from_wd|maintenance] [wiki]\n" ) ;
$command = $argv[1] ; //
$wiki = isset($argv[2])?$argv[2]:'' ;


function getQS () {
	global $qs ;
	if ( isset($qs) ) return $qs ;
	$toolname = '' ; // Or fill this in manually
	$path = realpath(dirname(__FILE__)) ;
	$user = get_current_user() ;
	if ( $toolname != '' ) {}
	else if ( preg_match ( '/^tools\.(.+)$/' , $user , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/data\/project\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/mnt\/nfs\/[^\/]+\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	if ( $toolname == '' ) die ( "getQS(): Can't determine the toolname for $path\n" ) ;
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/$toolname/bot.ini" ;
	$qs->toolname = 'SourceMD:CreateFromWikipediaDOIs' ;
	$qs->sleep = 0 ;
	$qs->generateAndUseTemporaryBatchID() ;
	return $qs ;
}

$ss = new ScienceSource ( $wiki ) ;

if ( $command == 'update_from_pageid' ) {
	if ( isset($argv[3]) ) $max_pages = $argv[3]*1 ;

	$wikis = [] ;
	if ( $wiki == 'all' ) {
		$result = $ss->getSQL ( "SELECT DISTINCT wiki FROM last_page_id" ) ;
		while($o = $result->fetch_object()) $wikis[] = $o->wiki ;
	} else $wikis = [ $wiki ] ;

	foreach ( $wikis AS $wiki ) {
		$ss = new ScienceSource ( $wiki ) ;
		$last_page_id = 0 ;
		$sql = "SELECT * FROM `last_page_id` WHERE `wiki`='{$ss->wiki_safe}'" ;
		$result = $ss->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $last_page_id = $o->wiki_page_id ;

		$sql = "SELECT * FROM page WHERE page_namespace=0 AND page_is_redirect=0 AND page_id>{$last_page_id} ORDER BY page_id" ;
		if ( isset($max_pages) ) $sql .= " LIMIT {$max_pages}" ;
		$dbw = $ss->getDBwiki() ;
		$result = $ss->tfc->getSQL ( $dbw , $sql ) ;
		while($o = $result->fetch_object()){
			$id = $ss->getOrCreatePage ( $o->page_id , $o->page_namespace , $o->page_latest , $o->page_title ) ;
			$ss->updatePublicationsForPage ( $id , $o->page_title , $o->page_namespace ) ;
			$ss->getSQL ( "INSERT INTO `last_page_id` (wiki,wiki_page_id) VALUES ('{$ss->wiki_safe}',{$o->page_id}) ON DUPLICATE KEY UPDATE wiki_page_id={$o->page_id}" ) ; # Update counter
		}
	}


} else if ( $command == 'update_from_rc' ) {

	if ( $wiki == 'all' ) {
		$result = $ss->getSQL ( "SELECT DISTINCT wiki FROM last_rc_timestamp" ) ;
		while($o = $result->fetch_object()){
			$ss2 = new ScienceSource ( $o->wiki ) ;
			$ss2->updateWikiRC () ;
		}
	} else {
		$ss->updateWikiRC () ;
	}

} else if ( $command == 'update_pubs_from_wd' ) {
	$ss->updatePublicationsFromWikidata () ;

} else if ( $command == 'merge_duplicate_publications' ) {
	$ss->mergeDuplicatePublications () ;

} else if ( $commands = 'load_dois_into_wikidata' ) {
#	$create_new_authors = true ;
	$had_doi = [] ;
	$result = $ss->getSQL ( "SELECT (select count(*) FROM pub2page WHERE publication_id=publication.id) AS cnt,publication.* FROM publication WHERE q IS NULL and doi IS NOT NULL AND failed_to_create=0 AND duplicate_q IS NULL ORDER BY cnt DESC LIMIT 5000" ) ;
	while($o = $result->fetch_object()){
#print "Checking {$o->doi}\n" ;
		$doi = trim ( str_replace('"','',$o->doi) ) ;
		if ( $doi == '' ) {
			$ss->getSQL ( "UPDATE publication SET failed_to_create=1 WHERE id={$o->id}" ) ;
			continue ;
		}
		if ( isset($had_doi[strtolower($doi)]) ) continue ;
		$had_doi[strtolower($doi)] = 1 ;
		$q = getOrCreateWorkFromIDs ( ['doi'=>$doi] ) ;
		if ( !isset($q) or $q == '' ) {
			$ss->getSQL ( "UPDATE publication SET failed_to_create=1 WHERE id={$o->id}" ) ;
			continue ;
		}
		$qnum = preg_replace('/\D/','',$q) ;

		# Using that q already?
		$result2 = $ss->getSQL ( "SELECT * FROM publication WHERE q={$qnum}" ) ;
		if ( $o2 = $result2->fetch_object() ) {
			$ss->getSQL ( "UPDATE publication SET duplicate_q={$qnum} WHERE id={$o->id}" ) ;
			continue ;
		}

		$sql = "UPDATE publication SET q={$qnum} WHERE id={$o->id}";
		try {
			$ss->getSQL ( $sql ) ;
		} catch (Exception $e) {
			print_r ( $e ) ;
			continue ;
		}
#		addOrCreateAutorsForPaper ( $doi , $q ) ;
		print "$doi\thttps://www.wikidata.org/wiki/{$q}\n" ;
	}
	$mnm->mergeDuplicatePublications() ;

} else if ( $commands = 'maintenance' ) {
	# Remove deleted pages and their publication links
	$ss->getSQL ( "DELETE FROM pub2page WHERE page_id IN (SELECT id FROM page WHERE wiki_revision_id=0)" ) ;
	$ss->getSQL ( "DELETE FROM page WHERE wiki_revision_id=0" ) ;

}

?>