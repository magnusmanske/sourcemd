<?PHP

require_once ( '/data/project/magnustools/public_html/php/ToolforgeCommon.php' ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;
#require_once ( '/data/project/sourcemd/sourcemd.php' ) ;

class ScienceSource {
	public $wiki , $wiki_safe ;
	public $db_tool , $db_wiki ;
	public $tfc ;
	public $bad_dois = [ '/^<a$/' , '/^" about=$/' ] ;

	public function __construct ( $wiki ) {
		$this->tfc = new ToolforgeCommon ( 'ScienceSource' ) ;
		$this->wiki = $wiki ;
		$this->wiki_safe = $this->escape($wiki) ;
	}

	public function getDBwiki () {
		if ( !isset($this->db_wiki) ) $this->db_wiki = $this->tfc->openDBwiki ( $this->wiki ) ;
		return $this->db_wiki ;
	}

	public function getDB () {
		if ( !isset($this->db_tool) ) $this->db_tool = $this->tfc->openDBtool ( 'science_source_p' ) ;
		return $this->db_tool ;
	}

	public function getSQL ( $sql ) {
		$db = $this->getDB() ;
		return $this->tfc->getSQL ( $db , $sql ) ;
	}

	public function escape ( $s ) {
		$db = $this->getDB() ;
		return $db->real_escape_string ( $s ) ;
	}

	public function isBadDOI ( $doi ) {
		foreach ( $this->bad_dois AS $bad_doi ) {
			if ( preg_match ( $bad_doi , $doi ) ) return true ;
		}
		return false ;
	}

	public function updatePublicationsForPage ( $page_id , $page_title , $page_namespace ) {
		# Deactivate linked publications; reactivate later
		$sql = "UPDATE pub2page SET use_flag=0 WHERE page_id={$page_id}" ;
		$this->getSQL ( $sql ) ;

		if ( !$this->addPublicationsForPage ( $page_id , $page_title , $page_namespace ) ) return ;

		# Remove old publication links
		$sql = "DELETE FROM pub2page WHERE page_id={$page_id} AND use_flag=0" ;
		$this->getSQL ( $sql ) ;

		# Set publication counter
		$sql = "UPDATE page SET number_of_publications=(SELECT count(*) FROM pub2page WHERE page_id={$page_id} and use_flag=1) WHERE id={$page_id}" ;
		$this->getSQL ( $sql ) ;
	}

	public function getOrCreatePage ( $page_id , $page_namespace , $rev_id , $page_title ) {
		$id = 0 ;
		$sql = "SELECT * FROM page WHERE wiki='{$this->wiki_safe}' AND wiki_page_id={$page_id}" ;
		$result = $this->getSQL ( $sql ) ;
		if ( $x = $result->fetch_object()){
			$id = $x->id ;
			if ( $rev_id == 0 ) { # Page has been deleted
				$this->getSQL ( "DELETE FROM pub2page WHERE page_id={$id}" ) ;
				$this->getSQL ( "DELETE FROM page WHERE id={$id}" ) ;
				return 0 ;
			}

			# Is that actually a new revision?
			if ( $x->wiki_revision_id == $rev_id ) return $id ; # Deactivate for testing

			# Update things that potentially changed for the page; wiki and page_id remain stable
			$sql = "UPDATE page SET wiki_page_title='".$this->escape($page_title)."',wiki_revision_id={$rev_id},wiki_namespace={$page_namespace} WHERE id={$id}" ;
			$this->getSQL ( $sql ) ;
		} else {
			if ( $rev_id == 0 ) return 0 ; # Page has been deleted

			# Create new page entry
			$sql = "INSERT IGNORE INTO page (wiki,wiki_page_title,wiki_page_id,wiki_revision_id,wiki_namespace) VALUES ('{$this->wiki_safe}','".$this->escape($page_title)."',{$page_id},{$rev_id},{$page_namespace})" ;
			$this->getSQL ( $sql ) ;
			$id = $this->getDB()->insert_id ;
		}
		if ( $id == 0 ) die ( "Page not available/created!\n" ) ; # Paranoia
		return $id ;
	}

	private function getOrCreatePublication ( $params ) {
		$sql_params = [] ;
		foreach ( $params AS $k => $v ) $sql_params[] = "`{$k}`='".$this->escape($v)."'" ;
		if ( count($sql_params) == 0 ) die ( "getOrCreatePublication called without parameters\n" ) ; # Paranoia
		$sql = "SELECT * FROM publication WHERE (" . implode(') OR (',$sql_params) . ")" ;
		$pubs = [] ;
		$result = $this->getSQL ( $sql ) ;
		while ( $x = $result->fetch_object()) $pubs[] = $x ;
		if ( count($pubs) > 1 ) { # Paranoia
			print_r ( $params ) ;
			print_r ( $pubs ) ;
			die ( "getOrCreatePublication: Too many results\n" ) ;
		}
		if ( count($pubs) == 1 ) return $pubs[0]->id ;

		$ks = [] ;
		$vs = [] ;
		foreach ( $params AS $k => $v ) {
			$ks[] = "`{$k}`" ;
			$vs[] = "'".$this->escape($v)."'" ;
		}
		$sql = "INSERT IGNORE INTO publication (" . implode(',',$ks) . ") VALUES (" . implode(',',$vs) . ")" ;
		$this->getSQL ( $sql ) ;
		$publication_id = $this->getDB()->insert_id ;
		if ( !isset($publication_id) ) die ( "getOrCreatePublication: Could not create publication with {$sql}\n" ) ; # Paranoia
		$this->updateSinglePublicationFromWikidata ( $publication_id ) ;
		return $publication_id ;
	}

	public function getOrCreatePublicationByDOI ( $doi ) {
		$doi = preg_replace ( '/^\s*doi\s*/' , '' , $doi ) ;
		$doi = str_replace ( "\n" , '' , $doi ) ;
		$doi = preg_replace ( '/["<].*$/' , '' , $doi ) ;
		$doi = preg_replace ( '/^([^"]+)"$/' , '$1' , $doi ) ;
		if ( $this->isBadDOI ( $doi ) ) return 0 ;
		return $this->getOrCreatePublication ( [ 'doi' => strtolower($doi) ] ) ;
	}

	public function getOrCreatePublicationByPMID ( $pmid ) {
		return $this->getOrCreatePublication ( [ 'pmid' => $pmid ] ) ;
	}

	public function getOrCreatePublicationByPMCID ( $pmcid ) {
		return $this->getOrCreatePublication ( [ 'pmcid' => $pmcid ] ) ;
	}

	public function addPublicationsForPage ( $page_id , $wiki_page_title , $page_namespace = 0 ) {
		if ( $page_namespace > 0 ) die ( "Non-0 namespace not supported yet\n") ;
		$server = $this->tfc->getWebserverForWiki ( $this->wiki ) ;
		$url = "https://{$server}/api/rest_v1/page/html/" . (str_replace(' ','_',$wiki_page_title)) ;
		$html = @file_get_contents ( $url ) ;
		if ( !isset($html) or $html === null or $html == '' ) return false ; // Non-existant/empty page

		# DOIs
		if ( false !== preg_match_all ( '/\bhref="(http:|https:){0,1}\/\/(dx\.){0,1}doi\.org\/(.+?)"/' , $html , $m ) ) {
			foreach ( $m[3] AS $doi ) {
				$doi = urldecode ( $doi ) ;
				$pub_id = $this->getOrCreatePublicationByDOI ( $doi ) ;
				$sql = "INSERT INTO pub2page(page_id,publication_id,use_flag) VALUES ({$page_id},{$pub_id},1) ON DUPLICATE KEY UPDATE use_flag=1" ;
				if ( $pub_id != 0 ) $this->getSQL ( $sql ) ;
			}
		}
		if ( false !== preg_match_all ( '/\bdoi:(\S+)/' , $html , $m ) ) {
			foreach ( $m[1] AS $doi ) {
				$doi = preg_replace ( '/[\.)\]]$/' , '' , $doi ) ; # Trailing char
				$pub_id = $this->getOrCreatePublicationByDOI ( $doi ) ;
				$sql = "INSERT INTO pub2page(page_id,publication_id,use_flag) VALUES ({$page_id},{$pub_id},1) ON DUPLICATE KEY UPDATE use_flag=1" ;
				if ( $pub_id != 0 ) $this->getSQL ( $sql ) ;
			}
		}

		# PubMed
		if ( false !== preg_match_all ( '/\bncbi\.nlm\.nih\.gov\/pubmed\/(\d+)"/' , $html , $m ) ) {
			foreach ( $m[1] AS $pmid ) {
				$pub_id = $this->getOrCreatePublicationByPMID ( $pmid ) ;
				$sql = "INSERT INTO pub2page(page_id,publication_id,use_flag) VALUES ({$page_id},{$pub_id},1) ON DUPLICATE KEY UPDATE use_flag=1" ;
				if ( $pub_id != 0 ) $this->getSQL ( $sql ) ;
			}
		}
		if ( false !== preg_match_all ( '/\bPMID[ :](\d+)"/' , $html , $m ) ) {
			foreach ( $m[1] AS $pmid ) {
				$pub_id = $this->getOrCreatePublicationByPMID ( $pmid ) ;
				$sql = "INSERT INTO pub2page(page_id,publication_id,use_flag) VALUES ({$page_id},{$pub_id},1) ON DUPLICATE KEY UPDATE use_flag=1" ;
				if ( $pub_id != 0 ) $this->getSQL ( $sql ) ;
			}
		}

		# PubMedCentral
		if ( false !== preg_match_all ( '/\bncbi\.nlm\.nih\.gov\/pmc\/articles\/PMC(\d+)"/' , $html , $m ) ) {
			foreach ( $m[1] AS $pmcid ) {
				$pub_id = $this->getOrCreatePublicationByPMCID ( $pmcid ) ;
				$sql = "INSERT INTO pub2page(page_id,publication_id,use_flag) VALUES ({$page_id},{$pub_id},1) ON DUPLICATE KEY UPDATE use_flag=1" ;
				if ( $pub_id != 0 ) $this->getSQL ( $sql ) ;
			}
		}
		if ( false !== preg_match_all ( '/\bPMC[ :](\d+)"/' , $html , $m ) ) {
			foreach ( $m[1] AS $pmcid ) {
				$pub_id = $this->getOrCreatePublicationByPMCID ( $pmcid ) ;
				$sql = "INSERT INTO pub2page(page_id,publication_id,use_flag) VALUES ({$page_id},{$pub_id},1) ON DUPLICATE KEY UPDATE use_flag=1" ;
				if ( $pub_id != 0 ) $this->getSQL ( $sql ) ;
			}
		}

		return true ;
	}

	public function updateWikiRC () {
		$last_rc_timestamp = '' ;
		$sql = "SELECT * FROM `last_rc_timestamp` WHERE `wiki`='{$this->wiki_safe}'" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $last_rc_timestamp = $o->timestamp ;

		$last_ts = '' ;
		$sql = "SELECT * FROM recentchanges WHERE rc_namespace=0" ;
		if ( $last_rc_timestamp != '' ) $sql .= " AND rc_timestamp>='{$last_rc_timestamp}'" ;
#	print "{$sql}\n" ;
		$dbw = $this->getDBwiki() ;
		$result = $this->tfc->getSQL ( $dbw , $sql ) ;
		while($o = $result->fetch_object()){
			$page_namespace = $o->rc_namespace ;
			$page_title = $o->rc_title ;
			$page_id = $o->rc_cur_id ;
			$rev_id = $o->rc_this_oldid ;

			$id = $this->getOrCreatePage ( $page_id , $page_namespace , $rev_id , $page_title ) ;
			if ( $id == 0 ) continue ; # TODO error message?
			$this->updatePublicationsForPage ( $id , $page_title , $page_namespace ) ;

			if ( $o->rc_timestamp > $last_ts ) $last_ts = $o->rc_timestamp ;
		}

		if ( $last_ts != '' ) {
			$sql = "INSERT INTO last_rc_timestamp (wiki,`timestamp`) VALUES ('{$this->wiki_safe}','{$last_ts}') ON DUPLICATE KEY UPDATE `timestamp`='{$last_ts}'" ;
			$this->getSQL ( $sql ) ;
		}
	}

	private function updateSinglePublicationFromWikidata ( $publication_id ) {
		$sql = "SELECT * FROM `publication` WHERE id={$publication_id}" ;
		$result = $this->getSQL ( $sql ) ;
		if ( $x = $result->fetch_object()) {}
		else return ; // No such ID
		$parts = [] ;
		if ( isset($x->doi) ) $parts[] = "VALUES ?doi { '".$this->escape(strtolower($x->doi))."' '".$this->escape(strtoupper($x->doi))."' } . ?q wdt:P356 ?doi" ;
		else $parts[] = "OPTIONAL {?q wdt:P356 ?doi}" ;
		if ( isset($x->pmid) ) $parts[] = "VALUES ?pmid { '".$this->escape($x->pmid)."' } . ?q wdt:P698 ?pmid" ;
		else $parts[] = "OPTIONAL {?q wdt:P698 ?pmid}" ;
		if ( isset($x->pmcid) ) $parts[] = "VALUES ?pmcid { '".$this->escape($x->pmcid)."' } . ?q wdt:P932 ?pmcid" ;
		else $parts[] = "OPTIONAL {?q wdt:P932 ?pmcid}" ;
		$sparql = "SELECT ?q ?doi ?pmid ?pmcid { " . implode ( ' . ' , $parts ) . " }" ;
		$j = $this->tfc->getSPARQL ( $sparql ) ;
		if ( count($j->results->bindings) == 0 ) return ; // Nothing on Wikidata
		if ( count($j->results->bindings) > 1 ) {
			# TODO error message/log
			return ;
		}
		$b = $j->results->bindings[0] ;
		$params = [ "`q`=" . preg_replace ( '/\D/' , '' , $this->tfc->parseItemFromURL ( $b->q->value ) ) ] ;
		foreach ( ['doi','pmcid','pmid'] AS $k ) {
			if ( isset($x->$k) ) continue ;
			if ( isset($b->$k) ) $params[] = "`{$k}`='" . $this->escape(strtolower($b->$k->value)) . "'" ;
		}
		$sql = "UPDATE `publication` SET " . implode(',',$params) . " WHERE id={$x->id}" ;
		$this->getSQL ( $sql ) ;
	}

	public function updatePublicationsFromWikidata () {
		$sql = "SELECT id FROM `publication` WHERE q IS NULL" ;
		$result = $this->getSQL ( $sql ) ;
		while ( $x = $result->fetch_object()) {
			$this->updateSinglePublicationFromWikidata ( $x->id ) ;
		}
	}

	public function mergeDuplicatePublications () {
		$result = $this->getSQL ( "select p1.*,(SELECT p2.id FROM publication p2 WHERE p2.q=p1.duplicate_q) AS id2 FROM publication p1 WHERE p1.duplicate_q IS NOT NULL" ) ;
		while ( $o = $result->fetch_object() ) {
			$this->getSQL ( "UPDATE IGNORE pub2page SET publication_id={$o->id2} WHERE publication_id={$o->id}" ) ;
			$this->getSQL ( "DELETE FROM pub2page WHERE publication_id={$o->id}" ) ;
			$this->getSQL ( "DELETE FROM publication WHERE id={$o->id}" ) ;
		}
	}

} ;

?>