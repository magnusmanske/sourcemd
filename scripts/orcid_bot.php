#!/usr/bin/php
<?PHP

require_once ( '/data/project/sourcemd/scripts/orcid_shared.php' ) ;

$max = 2000 ;
$create_new_authors = true ;



function getQS () {
	$toolname = '' ; // Or fill this in manually
	$path = realpath(dirname(__FILE__)) ;
	$user = get_current_user() ;
	if ( $toolname != '' ) {}
	else if ( preg_match ( '/^tools\.(.+)$/' , $user , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/data\/project\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/mnt\/nfs\/[^\/]+\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	if ( $toolname == '' ) die ( "getQS(): Can't determine the toolname for $path\n" ) ;
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/$toolname/bot.ini" ;
	$qs->toolname = 'SourceMD:ORCIDbot' ;
	$qs->sleep = 2 ;
	return $qs ;
}

function processPaper ( $q , $doi ) {
	global $process_paper_cache , $wil ;
	if ( !isset($wil) ) $wil = new WikidataItemList () ;
	if ( !isset($process_paper_cache) ) $process_paper_cache = [] ;

	$matches = addOrCreateAutorsForPaper ( $doi , $q ) ;
	$wil->loadItems ( array_keys ( $matches ) ) ; // Should already be loaded, just making sure...
	$commands = '' ;
	foreach ( $matches AS $q => $orcid ) {
		$i = $wil->getItem ( $q ) ;
		if ( $i->hasClaims ( 'P496' ) ) continue ;
		if ( isset ( $process_paper_cache[$q] ) ) continue ;
		$process_paper_cache[$q] = $orcid ;
		$commands .= "$q\tP496\t\"$orcid\"\n" ;
	}
	if ( $commands == '' ) return ; // Nothing to do
	$qs = getQS() ;
	$tmp = $qs->importData ( $commands , 'v1' ) ;
	$qs->runCommandArray ( $tmp['data']['commands'] ) ;
}

function incrementalUpdate ( $limit ) {
	global $dbwd , $dbt , $wil , $authors_with_orcid ;
	if ( count($authors_with_orcid) == 0 ) initAuthorsWithORCID() ;
	if ( !isset($dbt) )  $dbt  = openToolDB ( 'sourcemd_orcid_p') ;
	if ( !isset($dbwd) ) $dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
	if ( !isset($wil) ) $wil = new WikidataItemList () ;

	// Init
	$q2pageid = [] ;
	$last = 0 ;
	$sql = "SELECT * FROM `meta` WHERE `key`='mode4_last_pageid'" ;
	$result = getSQL ( $dbt , $sql  ) ;
	while($o = $result->fetch_object()) $last = $o->value * 1 ;

	// Get next items to process
#	$sql = "SELECT DISTINCT pl_from,page_title FROM page,pagelinks WHERE pl_from=page_id AND pl_namespace=120 AND pl_from_namespace=0 AND pl_title='P356' AND pl_from>$last ORDER BY pl_from LIMIT $limit" ;
	$page_ids = [] ;
	$sql = "SELECT DISTINCT pl_from FROM pagelinks WHERE pl_namespace=120 AND pl_from_namespace=0 AND pl_title='P356' AND pl_from>$last ORDER BY pl_from LIMIT $limit" ;
	$result = getSQL ( $dbwd , $sql ) ;
	while($o = $result->fetch_object()) $page_ids[] = $o->pl_from ;
	if ( count($page_ids) == 0 ) return 0 ;

	$sql = "SELECT page_id,page_title FROM page WHERE page_id IN (" . implode(',',$page_ids) . ")" ;
	$items = [] ;
	$result = getSQL ( $dbwd , $sql ) ;
	while($o = $result->fetch_object()) {
		$q = $o->page_title ;
		$pid = $o->page_id ;
		$q2pageid[$q] = $pid ;
		$items[] = $q ;
	}
	if ( count($items) == 0 ) return 0 ; # Paranoia

	// Get DOIs for items
	$q2doi = [] ;
	$sparql = "SELECT DISTINCT ?q ?doi { VALUES ?q { wd:" . implode ( " wd:" , $items ) . " } . ?q wdt:P356 ?doi }" ;
	$j = getSPARQL ( $sparql ) ;
	if ( !isset($j) or !isset($j->results) or !isset($j->results->bindings) ) {
		print "SPARQL problem:\n$sparql\n" ;
		print_r ( $j ) ;
		exit ( 0 ) ;
	}
	foreach ( $j->results->bindings AS $b ) {
		$q = preg_replace ( '/^.+\/Q/' , 'Q' , $b->q->value ) ;
		$q2doi[$q] = $b->doi->value ;
	}
	$wil->loadItems ( array_keys ( $q2doi ) ) ; // Pre-cache items

	// Process items
	foreach ( $items AS $q ) {
		if ( !isset($q2pageid[$q]) or !isset($q2doi[$q]) ) continue ;
		$page_id = $q2pageid[$q] ;
		$doi = $q2doi[$q] ;
		$i = $wil->getItem ( $q ) ;
		if ( !isset($i) ) continue ;
		if ( !$i->hasTarget ( 'P31' , 'Q13442814' ) ) continue ;
#print "$q\t$page_id\t$doi\n" ;
		processPaper ( $q , $doi ) ;

	}

	// Update current position
	$q = $items[count($items)-1] ;
	$page_id = $q2pageid[$q] ;
	$sql = "UPDATE `meta` SET `value`='{$page_id}' WHERE `key`='mode4_last_pageid'" ;
	getSQL ( $dbt , $sql ) ;

	return count($items) ;
}

$mode = '' ;
if ( isset($argv[1]) ) $mode = $argv[1] ;

$wil = new WikidataItemList () ;

initAuthorsWithORCID () ;

$dois = array() ;

if ( isset($argv[1]) and preg_match ( '/^Q\d+$/' , $argv[1] ) ) {
	$q = $argv[1] ;
	$wil->loadItem ( $q ) ;
	$i = $wil->getItem ( $q ) ;
	if ( $i->hasClaims('P356') ) {
		$dois[$q] = $i->getFirstString ( 'P356' ) ;
	} else {
		die ( "Cannot determine argument $q\n" ) ;
	}
}

//$dois = array( 'Q20901697' => '10.1038/nature11174' ) ;
//$dois = array ( 'Q21144700' => '10.1371/JOURNAL.PMED.0030308' ) ;

if ( $mode == '3' ) { // UPDATE AUTHORS
	$sparql = "SELECT ?q { ?q wdt:P31 wd:Q5 ; wdt:P496 [] } order by rand() LIMIT 5000" ;
	$items = getSPARQLitems ( $sparql ) ;

	$wil->loadItems ( $items ) ;
	foreach ( $items AS $q ) {
		$q = 'Q' . trim ( strtoupper ( $q ) ) ;
		$res = add_person_ids_from_orcid ( $q ) ;
	}
	exit(0);
}




if ( $mode == '4' ) { // SCAN ALL DOIs

	$limit = 100 ; # SPARQL limit, apparently
	while ( 1 ) {
		$wil = new WikidataItemList () ; // New one for every loop
		$processed = incrementalUpdate ( $limit ) ;

		if ( $processed < $limit ) break ;
	}

	exit(0) ;
}


if ( $mode == '5' ) { // FIX NAMED AUTHOR/AUTHOS DUPLICATES
	while ( 1 ) {
		$wil = new WikidataItemList () ;
		$commands = '' ;
		$sparql = 'SELECT ?paper ?author ?name ?series {
	  ?paper p:P2093 ?statement_na .
	  ?statement_na ps:P2093 ?name ; pq:P1545 ?series .
	  ?paper p:P50 ?statement_a .
	  ?statement_a ps:P50 ?author ; pq:P1545 ?series ; pq:P1932 ?name
	  }
	LIMIT 500';
		$j = getSPARQL ( $sparql ) ;
	//	print_r ( $j ) ;
		$to_load = [] ;
		foreach ( $j->results->bindings AS $b ) $to_load[] = preg_replace ( '/^.+\/Q/' , 'Q' , $b->paper->value ) ;
		$wil->loadItems ( $to_load ) ;
		foreach ( $j->results->bindings AS $b ) {
			$name = $b->name->value ;
			$paper_q = preg_replace ( '/^.+\/Q/' , 'Q' , $b->paper->value ) ;
			$i = $wil->getItem ( $paper_q ) ;
			if ( !isset($i) ) {
				print "No such paper $paper_q\n" ;
				continue ;
			}
			$named_authors = $i->getStrings ( 'P2093' ) ;
			$found = 0 ;
			foreach ( $named_authors AS $na ) {
				if ( $na == $name ) $found++ ;
			}
			if ( $found != 1 ) {
				print "Found $found entries for '$name' in $paper_q\n" ;
				continue ;
			}
			$commands .= "-{$paper_q}\tP2093\t\"{$name}\"\n" ;
		}
		$qs = getQS() ;
		$tmp = $qs->importData ( $commands , 'v1' ) ;
		$qs->runCommandArray ( $tmp['data']['commands'] ) ;
		sleep ( 60 * 10 ) ; // 10min
	}
	exit ( 0 ) ;
}


$tmp = array() ;
if ( $mode != '' ) { // Command line mode
	if ( $mode == '2' ) {
		$t = file_get_contents ( 'https://www.wikidata.org/w/api.php?action=feedcontributions&user=Research+Bot&namespace=0&newonly=1&limit=50' ) ;
		if ( !preg_match_all ( '/<title>(Q\d+)<\/title>/' , $t , $m ) ) die ( "Mode 2: No results\n" ) ;
		$tmp = $m[1] ;
		if ( count($tmp) == 0 ) die ( "Mode 2: No matches\n" ) ;
	}
}
if ( count($dois) == 0 ) { // No testing

	$sparql_mode = (rand(0,1) == 0) ; // Set to 1 for just random papers
	if ( $mode != '' ) $sparql_mode = $mode ;

	$sparql = '' ;
	if ( $sparql_mode == 1 ) { 
		# Just random papers
#		$sparql = "SELECT DISTINCT ?q ?doi { ?q wdt:P356 ?doi . ?q wdt:P31 wd:Q13442814 } ORDER BY rand() LIMIT $max" ;
		$sparql = "SELECT ?q ?doi { { SELECT DISTINCT ?q { ?q wdt:P31 wd:Q13442814 } ORDER BY rand() LIMIT $max } . ?q wdt:P356 ?doi }" ;
	} else if ( $sparql_mode == 0 ) {
		# Papers with living authors without ORCID
		$sparql = "SELECT DISTINCT ?q ?doi { ?q wdt:P31 wd:Q13442814 . ?q wdt:P356 ?doi . ?q wdt:P50 ?author . ?author wdt:P31 wd:Q5 . MINUS { ?author wdt:P496 [] } . MINUS { ?author wdt:P570 [] } } ORDER BY rand() LIMIT $max" ;
	} else if ( $sparql_mode == 2 ) {
		$sparql = "SELECT DISTINCT ?q ?doi { VALUES ?q { wd:" . implode ( " wd:" , $tmp ) . " } . ?q wdt:P356 ?doi }" ;
	}
	
	$j = getSPARQL ( $sparql ) ;
	if ( !isset($j) or !isset($j->results) or !isset($j->results->bindings) ) {
		print "SPARQL problem:\n$sparql\n" ;
		exit ( 0 ) ;
	}
	foreach ( $j->results->bindings AS $b ) {
		$q = preg_replace ( '/^.+\/Q/' , 'Q' , $b->q->value ) ;
		$dois[$q] = $b->doi->value ;
	}
}

$wil->loadItems ( array_keys ( $dois ) ) ; // Pre-cache

$set_cache = array() ;
foreach ( $dois AS $q => $doi ) {


#	print ( "Trying $q => $doi...\n" ) ;
	$matches = addOrCreateAutorsForPaper ( $doi , $q ) ;
	$wil->loadItems ( array_keys ( $matches ) ) ; // Should already be loaded, just making sure...
	$commands = '' ;
	foreach ( $matches AS $q => $orcid ) {
		$i = $wil->getItem ( $q ) ;
		if ( $i->hasClaims ( 'P496' ) ) {
//			logit ( "$q already has ORCID" ) ;
			continue ;
		}
		if ( isset ( $set_cache[$q] ) ) {
			continue ;
		}
		$set_cache[$q] = $orcid ;
		$commands .= "$q\tP496\t\"$orcid\"\n" ;
#print "ADDED\tORCID\t$q\t$orcid\n" ;
	}
	if ( $commands == '' ) continue ; // Nothing to do
	$qs = getQS() ;
	$tmp = $qs->importData ( $commands , 'v1' ) ;
	$qs->runCommandArray ( $tmp['data']['commands'] ) ;
}

?>
