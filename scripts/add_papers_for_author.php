#!/usr/bin/php
<?PHP

require_once ( '/data/project/sourcemd/scripts/orcid_shared.php' ) ;

$create_new_authors = true ;

function getQS () {
	$toolname = '' ; // Or fill this in manually
	$path = realpath(dirname(__FILE__)) ;
	$user = get_current_user() ;
	if ( $toolname != '' ) {}
	else if ( preg_match ( '/^tools\.(.+)$/' , $user , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/data\/project\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/mnt\/nfs\/[^\/]+\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	if ( $toolname == '' ) die ( "getQS(): Can't determine the toolname for $path\n" ) ;
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/$toolname/bot.ini" ;
	$qs->toolname = 'SourceMD:AddPapersForAuthor' ;
	$qs->sleep = 1 ;
	return $qs ;
}

#addOrCreateAutorsForPaper ( '10.1111/j.1471-0528.1995.tb10859.x' , 'Q52995596' ) ; exit(0) ;

if ( !isset($argv[1]) ) die ( "No ORCID ID given!\n" ) ;
$orcid = $argv[1] ; // Test:"0000-0002-5023-0176" ;

$o = new ORCID ;
$works = $o->getWorksForORCID ( $orcid ) ;
foreach ( $works AS $w ) {
	if ( !isset($w['doi']) ) continue ; // Paranoia
	$q = getOrCreateWorkFromIDs ( $w ) ;
}

?>