#!/usr/bin/php
<?PHP

require_once ( '/data/project/sourcemd/scripts/orcid_shared.php' ) ;

$logfile = '/data/project/sourcemd/public_html/dois.wikispecies.txt' ;

$create_new_authors = true ;

function getQS () {
	$toolname = '' ; // Or fill this in manually
	$path = realpath(dirname(__FILE__)) ;
	$user = get_current_user() ;
	if ( $toolname != '' ) {}
	else if ( preg_match ( '/^tools\.(.+)$/' , $user , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/data\/project\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/mnt\/nfs\/[^\/]+\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	if ( $toolname == '' ) die ( "getQS(): Can't determine the toolname for $path\n" ) ;
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/$toolname/bot.ini" ;
	$qs->toolname = 'SourceMD:CreateFromWikispeciesDOIs' ;
	$qs->sleep = 5 ;
	return $qs ;
}

function getDOIs () {
	global $tfc ;
	$ret = [] ;
	if ( !isset($tfc) ) $tfc = new ToolforgeCommon ;
	$db = $tfc->openDB ( 'wikispecies' , 'wikispecies' ) ;
	$sql = 'SELECT DISTINCT el_to FROM externallinks where el_to like "%doi%"' ;
	$result = $tfc->getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()){
		$row = $o->el_to ;
		$doi = '' ;
		$row = trim ( $row ) ;
		if ( preg_match ( '/(%2F|%7C)/' , $row ) ) $row = urldecode ( $row ) ;
		if ( preg_match ( '/chalcidoids/' , $row ) ) continue ;

		$row = preg_replace ( '/(;jsessionid|\#).*$/' , '' , $row ) ;
		$row = preg_replace ( '/\/(abstract|epdf|full|pdf|viewdoc|download|issuetoc)+\/*$/' , '' , $row ) ;
		if ( preg_match ( '/\.(pdf|php|html{0,1}|dsml)$/' , $row ) ) continue ;

		if ( preg_match ( '/\/doi\/[a-z]+\/(\d+\.\d+\/.+)$/' , $row , $m ) ) $doi = $m[1] ;
		else if ( preg_match ( '/[\/:\|\&\?]doi[\/=](\d+\.\d+\/.+)$/' , $row , $m ) ) $doi = $m[1] ;
		else if ( preg_match ( '/^https{0,1}:\/\/doi.org\/(.+)$/' , $row , $m ) ) $doi = $m[1] ;
		else if ( preg_match ( '/^https{0,1}:\/\/dx.doi.org\/(.+)$/' , $row , $m ) ) $doi = $m[1] ;
		else {
#			print "NO DOI FOR $row\n" ;
			continue ;
		}
		$doi = preg_replace ( '/\&.*/' , '' , $doi ) ;
		$doi = preg_replace ( '/^doi:/' , '' , $doi ) ;
		$doi = preg_replace ( '/\/+$/' , '' , $doi ) ;
		if ( !preg_match ( '/^\d+\.\d+\/.+$/' , $doi ) ) {
#			print "BAD DOI $doi FOR URL $row\n" ;
			continue ;
		}
		$ret[$doi] = $doi ;
	}
	return array_keys($ret) ;
}

$rows = getDOIs () ;
#print count( $rows ) ."\n" ; exit(0);

$out = fopen ( "{$logfile}.tmp" , 'w' ) ;
$o = new ORCID ;
foreach ( $rows AS $doi ) {
	$doi = trim ( $doi ) ;
	$k = 'DOI:'.strtolower($doi) ;
	if ( isset($had_that[$k]) ) continue ;
	$had_that[$k] = 1 ;
	if ( $doi == '' ) continue ;
	$q = getOrCreateWorkFromIDs ( ['doi'=>$doi] ) ;
	if ( !isset($q) or $q == '' ) continue ;
	addOrCreateAutorsForPaper ( $doi , $q ) ;
	fwrite ( $out , "{$doi}\t{$q}\n" ) ;
}
fclose ( $out ) ;

rename ( "{$logfile}.tmp" , $logfile ) ;

?>