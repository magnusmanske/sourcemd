<?php

require_once ( '/data/project/sourcemd/scripts/orcid_shared.php' ) ;

$qs = [] ;
$wil = new WikidataItemList () ;
$create_new_authors = true ;

function getQS () {
	global $qs ;
	return $qs ;
}

function runCommands ( $commands ) {
	global $qs ;
	$qs->use_command_compression = true ;
	$tmp = $qs->importData ( implode ( "\n" , $commands ) , 'v1' ) ;
	$tmp['data']['commands'] = $qs->compressCommands ( $tmp['data']['commands'] ) ;
	$qs->runCommandArray ( $tmp['data']['commands'] ) ;
	return $qs->last_item ;
}

class BatchBot {

	public $tfc , $dbt , $wil ;
	private $qs_cache = [] ;

	public function __construct () {
		global $wil , $tfc ;
		if ( !isset($wil) ) $wil = new WikidataItemList () ;
		$this->wil = $wil ;
		if ( !isset($tfc) ) $tfc = new ToolforgeCommon ('sourcemd') ;
		$this->tfc = $tfc ;
		$this->dbt = $this->tfc->openDBtool ( 'sourcemd_batches_p') ;
	}

	# Must not be cached!
	public function getBatchInfo ( $batch_id ) {
		$batch_id *= 1 ;
		$sql = "SELECT * FROM batch WHERE id={$batch_id}" ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
		while($o = $result->fetch_object()) return $o ;
	}

	private function updateBatch ( $batch_id , $key , $value ) {
		$batch_id *= 1 ;
		$sql = "UPDATE `batch` set `" . $this->dbt->real_escape_string($key) . "`='" . $this->dbt->real_escape_string($value) . "' WHERE id={$batch_id}" ;
		$this->tfc->getSQL ( $this->dbt , $sql ) ;
	}

	private function getQSforBatch ( $batch_id ) {
		$batch_id *= 1 ;
		if ( isset($this->qs_cache[$batch_id]) ) return $this->qs_cache[$batch_id] ;

		$batch = $this->getBatchInfo ( $batch_id ) ;
		$toolinfo = "sourcemd_batch_processing" ;
		if ( $batch->user != '' ) $toolinfo .= " on behalf of [[User:{$batch->user}|]] (batch {$batch_id})" ;
		$qs = $this->tfc->getQS($toolinfo,'/data/project/sourcemd/bot.ini',true) ;
#		$qs->sleep = 0 ;

		# Ensure consistent temporary batch ID for this batch
		if ( $batch->qs_temporary_batch_id == '' ) {
			$this->updateBatch ( $batch_id , 'qs_temporary_batch_id' , $qs->temporary_batch_id ) ;
		} else {
			$qs->temporary_batch_id = $batch->qs_temporary_batch_id ;
		}
		$this->qs_cache[$batch_id] = $qs ;
		return $qs ;
	}

	public function updateBatchStatus ( $batch_id ) {
		$batch_id *= 1 ;
		$sql = "SELECT `status`,count(*) AS cnt FROM `command` WHERE batch_id={$batch_id} GROUP BY `status`" ;
		$overview = [ 'TODO' => 0 , 'TOTAL' => 0 ] ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
		while($o = $result->fetch_object()) {
			$overview[$o->status] = $o->cnt * 1 ;
			$overview['TOTAL'] += $o->cnt * 1 ;
		}
		$sql = "UPDATE batch SET `overview`='". $this->dbt->real_escape_string(json_encode($overview))."',last_action=now()" ;
		if ( $overview['TODO'] == 0 ) $sql .= ",`status`='DONE'" ;
		else $sql .= ",`status`='TODO'" ;
		$sql .= " WHERE id={$batch_id} AND `status`!='STOPPED'" ;
		$this->tfc->getSQL ( $this->dbt , $sql ) ;
	}

	private function getMaxSerialNumberInBatch ( $batch_id ) {
		$batch_id *= 1 ;
		$sql = "SELECT max(serial_number) AS sn FROM `command` WHERE batch_id={$batch_id}" ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
		if($o = $result->fetch_object()) return $o->sn * 1 ;
	}

	private function failCommand ( &$c , $note = '' ) {
		$c->status = 'FAILED' ;
		if ( $note != '' ) $c->note = $note ;
		return false ;
	}

	public function createNewBatch ( $user , $name = '' ) {
		$d = $this->tfc->getCurrentTimestamp() ;
		$sql = "INSERT INTO batch (`user`,`date_created`,`name`,`last_action`) VALUES ('$user','$d','$name',now())" ;
		$this->tfc->getSQL ( $this->dbt , $sql ) ;
		$batch_id = $this->dbt->insert_id ;
		$this->updateBatchStatus ( $batch_id ) ;
		return $batch_id ;
	}

	public function addNewCommandToBatch ( $batch_id , $mode , $identifier , $auto_escalate = false ) {
		if ( $identifier == '[]' ) return ; // Empty JSON
		$batch_id *= 1 ;
		$new_serial_number = $this->getMaxSerialNumberInBatch ( $batch_id ) + 1 ;
		$sql = "INSERT IGNORE INTO `command` (batch_id,serial_number,mode,identifier,auto_escalate) VALUES ({$batch_id},{$new_serial_number},'".
			$this->dbt->real_escape_string($mode)."','".
			$this->dbt->real_escape_string($identifier)."',".($auto_escalate?1:0).")" ;
		$this->tfc->getSQL ( $this->dbt , $sql ) ;
		$this->updateBatchStatus ( $batch_id ) ;
	}






	private function runCommandCreatePaperByID ( &$c ) {
		global $action_taken , $debugging ;
		$action_taken = '' ;
		$id = $c->identifier ;
		if ( substr($id,0,1) == '{' ) $id = (array) json_decode ( $id ) ;
		else $id = [ 'doi' => $id , 'pmid' => $id , 'pmcid' => $id ] ;
if($debugging) print "Checking work for " . json_encode($id) . " ...\n" ;
		$work_q = getOrCreateWorkFromIDs ( $id , false ) ;
if($debugging) print "...done! => https://www.wikidata.org/wiki/{$work_q}\n" ;
		$c->note = $action_taken ;
		if ( !isset($work_q) ) return $this->failCommand ( $c , "Couldn't create an item for ID '{$c->identifier}'" ) ;
		$c->q = $work_q ;
		$c->status = 'DONE' ;

		if ( $c->auto_escalate and isset($work_q) ) {
			$this->addNewCommandToBatch ( $c->batch_id , 'ADD_AUTHOR_TO_PUBLICATION' , $work_q ) ; # Fixup authors
		}
	}

	private function runCommandAddAuthorToPublication ( &$c ) {
		global $authors_with_orcid ;
		if ( !isset($authors_with_orcid) or count($authors_with_orcid) == 0 ) initAuthorsWithORCID() ;
		$q = $c->identifier ;
		$list = [ $q ] ;
		$q2doi = qs2dois ( $list ) ;
		if ( !isset($q2doi[$q]) ) return $this->failCommand ( $c , "Couldn't find DOI for {$q}'" ) ;
		$this->wil->loadItems ( [$q] ) ;
		$doi = $q2doi[$q] ;

		$set_cache = [] ;
		$matches = addOrCreateAutorsForPaper ( $doi , $q ) ;
		$this->wil->loadItems ( array_keys ( $matches ) ) ; // Should already be loaded, just making sure...
		$commands = '' ;
		foreach ( $matches AS $q => $orcid ) {
			$i = $this->wil->getItem ( $q ) ;
			if ( $i->hasClaims ( 'P496' ) ) {
				continue ;
			}
			if ( isset ( $set_cache[$q] ) ) continue ;
			$set_cache[$q] = $orcid ;
			$commands .= "$q\tP496\t\"$orcid\"\n" ;
		}
		if ( $commands == '' ) {
			$c->note = 'Nothing to do' ;
		} else {
			if ( is_array($commands) ) $c->note = implode ( "\n" , $commands ) ;
			$qs = getQS() ;
			$tmp = $qs->importData ( $commands , 'v1' ) ;
			$qs->runCommandArray ( $tmp['data']['commands'] ) ;
		}
		$c->status = 'DONE' ;

	}

	private function getOrCreateAuthorItemFromOrcid ( $orcid ) {
		$sparql = "SELECT ?q { ?q wdt:P496 '{$orcid}' }" ;
		$items = $this->tfc->getSPARQLitems ( $sparql ) ;
		if ( count($items) == 1 ) return $items[0] ; # Found it!
		if ( count($items) > 1 ) return '' ; # Too many!!

		# OK, create a new one
		$oc = new ORCID ;
		if ( !$oc->isValidORCID($orcid) ) return '' ; // Bad ORCID ID

		$person = $oc->getPersonInfoORCID ( $orcid ) ;
		if ( !isset($person) or $person === null ) return '' ;
#		$names = $oc->parsePersonAliasesORCID ( $person ) ;

		$name = '' ;
		if ( isset($person->person->name) ) {
			$n = $person->person->name ;
			if ( isset($n->{'credit-name'}) ) {
				$name = $n->{'credit-name'}->value ;
			} else {
				if ( isset($n->{'given-names'}) ) $name .= $n->{'given-names'}->value . ' ' ;
				if ( isset($n->{'family-name'}) ) $name .= $n->{'family-name'}->value ;
				$name = trim($name) ;

			}
		} ;

		$author_q = getOrCreateAuthorItem ( $name , $orcid , $person ) ;
		if ( !isset($author_q) or $author_q == '' ) return '' ; // Paranoia
	
 		add_person_ids_from_orcid ( $author_q ) ;
 		return $author_q ;
	}

	public function getAuthorQForID ( $id ) {
		if ( preg_match ( '/^Q\d+$/i' , $id ) ) return $id ; # Item ID
		if ( preg_match ( '/^\d{4}-\d{4}-\d{4}-\S{4}$/' , $id ) ) { # ORCID
			return $this->getOrCreateAuthorItemFromOrcid($id) ;
		}
		return '' ; # This is a bad ID
	}

	private function runCommandAddMetadataFromOrcidAuthor ( &$c ) {
		$q = $this->getAuthorQForID ( $c->identifier ) ;
		if ( $q == '' ) return $this->failCommand ( $c , "Invalid author ID'" ) ;
		$this->wil->loadItems ( [$q] ) ;
		$q = trim ( strtoupper ( $q ) ) ;
		if ( $q == '' ) return $this->failCommand ( $c , "Invalid identifier '{$q}'" ) ;
		if ( !preg_match ( '/^Q\d+$/' , $q ) ) return $this->failCommand ( $c , "'{$q}' is not a Wikidata item" ) ;
		$res = add_person_ids_from_orcid ( $q ) ;
		$c->status = 'DONE' ; # Really?
		$c->note = $res ;
	}

	private function runCommandEditPaperForOrcidAuthor ( &$c ) {
		$q = $this->getAuthorQForID ( $c->identifier ) ;
		if ( $q == '' ) return $this->failCommand ( $c , "Invalid author ID'" ) ;
		$o = new ORCID ;
		$this->wil->loadItems ( [$q] ) ;
		$i = $this->wil->getItem ( $q ) ;
		if ( !isset($i) ) return $this->failCommand ( $c , "Could not find Wikidata item '{$q}'" ) ;
		$orcid = $i->getFirstString ( 'P496' ) ;
		if ( !$o->isValidORCID($orcid) ) return $this->failCommand ( $c , "'{$q}': '{$orcid}' is not a valid ORCID ID" ) ;
		$q = $i->getQ() ;

		$works = $o->getWorksForORCID ( $orcid ) ;

		# Get IDs for works bu this author (P50); no need to check those
		$sparql = "SELECT ?doi ?pmid ?pmc { ?work wdt:P50 wd:Q27664356 OPTIONAL { ?work wdt:P356 ?doi } OPTIONAL { ?work wdt:P698 ?pmid } OPTIONAL { ?work wdt:P932 ?pmc } }" ;
		$j = $this->tfc->getSPARQL ( $sparql ) ;
		$ids = [ 'doi'=>[] , 'pmc'=>[] , 'pmid'=>[] ] ;
		foreach ( $j->results->bindings AS $v ) {
			if ( isset($v->doi) ) $ids['doi'][strtoupper($v->doi->value)]=1 ;
			if ( isset($v->pmid) ) $ids['pmid'][$v->pmid->value] = 1 ;
			if ( isset($v->pmc) ) $ids['pmc'][preg_replace('/^\D+/','',$v->pmc->value)] = 1 ;
		}
		foreach ( $works AS $w ) {
			if ( count($w) == 0 ) continue ; // No IDs to work with
			if ( isset($w['doi']) and isset($ids['doi'][strtoupper($w['doi'])]) ) continue ; // Already on WD
			if ( isset($w['pmid']) and isset($ids['pmid'][$w['pmid']]) ) continue ; // Already on WD
			if ( isset($w['pmc']) and isset($ids['pmc'][preg_replace('/^\D+/','',$w['pmc'])]) ) continue ; // Already on WD
			$this->addNewCommandToBatch ( $c->batch_id , 'CREATE_PAPER_BY_ID' , json_encode($w) , true ) ; # Add new "create work from ID" command to this batch, and auto-escalate
		}
		$c->status = 'DONE' ;
	}

	private function runCommandAddBookViaISBN ( &$c ) {
		global $qs ;
		$parts = explode ( '|' , $c->identifier , 2 ) ;
		$parts[0] = trim ( $parts[0] ) ;
		if ( count($parts) == 1 ) $parts[] = getBookTitleFromISBN ( $parts[0] ) ;
		if ( $parts[1] == '' ) $parts[1] = $parts[0] ; # Fallback title==ISBN
		$qs = $this->getQSforBatch ( $c->batch_id ) ;
		$q = getOrCreateBookFromISBN ( $parts[0] , $parts[1] ) ;
		if ( isset($q) and $q != 'LAST' and $q != '' ) {
			$c->status = 'DONE' ;
			$c->q = $q ;
		} else {
			return $this->failCommand ( $c , "Could not create Wikidata item for '{$parts[0]}' / '{$parts[1]}'" ) ;
		}
	}

	// Takes an object respresenting a `command` row
	public function runCommand ( $c ) {
		unset($c->note) ;
		$c->q = '' ;
		global $qs ;
		$qs = $this->getQSforBatch ( $c->batch_id ) ;
		switch ( $c->mode ) {
			case 'CREATE_PAPER_BY_ID' : $this->runCommandCreatePaperByID($c) ; break ;
			case 'ADD_AUTHOR_TO_PUBLICATION' : $this->runCommandAddAuthorToPublication($c) ; break ;
			case 'ADD_METADATA_FROM_ORCID_TO_AUTHOR' : $this->runCommandAddMetadataFromOrcidAuthor($c) ; break ;
			case 'EDIT_PAPER_FOR_ORCID_AUTHOR' : $this->runCommandEditPaperForOrcidAuthor($c) ; break ;
			case 'CREATE_BOOK_FROM_ISBN' : $this->runCommandAddBookViaISBN($c) ; break ;
		}
		$sql = "UPDATE `command` SET `status`='".$this->dbt->real_escape_string($c->status)."'" ;
		if ( isset($c->note) and $c->note !== null and $c->note != '' ) $sql .= ",`note`='".$this->dbt->real_escape_string($c->note)."'" ;
		else $sql .= ",`note`=null" ;
		$sql .= ",`q`='".$this->dbt->real_escape_string($c->q)."'" ;
		$sql .= " WHERE id={$c->id}" ;
		$this->tfc->getSQL ( $this->dbt , $sql ) ;
		$this->updateBatchStatus ( $c->batch_id ) ;
	}


	// Runs the next TODO command for each active batch
	public function runNextCommands ( $specific_batch ) {
		# Reset Wikidata, save memory
		global $wil , $debugging ;
		$wil = new WikidataItemList () ;
		$this->wil = $wil ;

		# Change batches from TODO to DONE if there is nothing more to do
		$sql = "SELECT * FROM batch WHERE `status`='TODO' AND NOT EXISTS (SELECT * FROM `command` WHERE batch_id=batch.id AND `command`.`status`='TODO' LIMIT 1)" ;
		if ( isset($specific_batch) ) $sql .= " AND id={$specific_batch}" ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
		while($o = $result->fetch_object()) $this->updateBatchStatus ( $o->id ) ;

		$cnt = 0 ;
		$sql = "SELECT batch_id,min(serial_number) AS sn FROM `command` WHERE " ;
		if ( isset($specific_batch) ) $sql .= " batch_id={$specific_batch} AND " ;
		$sql .= " `status`='TODO' AND NOT EXISTS (SELECT * FROM batch WHERE batch.id=batch_id AND `status`='STOPPED') GROUP BY batch_id" ;
if ( $debugging ) print "$sql\n" ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
		while($o = $result->fetch_object()){
			$sql = "SELECT * FROM `command` WHERE batch_id={$o->batch_id} AND serial_number={$o->sn} AND `status`='TODO' LIMIT 1" ;
			$result2 = $this->tfc->getSQL ( $this->dbt , $sql ) ;
			while($o2 = $result2->fetch_object()) {
if($debugging) print_r ( $o2 ) ;
				$sql = "UPDATE `command` SET `status`='RUNNING' WHERE id={$o2->id}" ;
				$this->tfc->getSQL ( $this->dbt , $sql ) ;
				$o2->status = 'RUNNING' ;
				$this->updateBatchStatus ( $o2->batch_id ) ;
				$this->runCommand ( $o2 ) ;
			}
			$cnt++ ;
		}
		return $cnt ; // Active batches
	}

} ;


?>