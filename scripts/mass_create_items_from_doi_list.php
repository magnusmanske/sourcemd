#!/usr/bin/php
<?PHP

require_once ( '/data/project/sourcemd/scripts/orcid_shared.php' ) ;

$create_new_authors = true ;

function getQS () {
	$toolname = '' ; // Or fill this in manually
	$path = realpath(dirname(__FILE__)) ;
	$user = get_current_user() ;
	if ( $toolname != '' ) {}
	else if ( preg_match ( '/^tools\.(.+)$/' , $user , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/data\/project\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/mnt\/nfs\/[^\/]+\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	if ( $toolname == '' ) die ( "getQS(): Can't determine the toolname for $path\n" ) ;
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/$toolname/bot.ini" ;
	$qs->toolname = 'SourceMD:CreateFromWikipediaDOIs' ;
	$qs->sleep = 5 ;
	return $qs ;
}

$file = $argv[1] ;
if ( !isset($file) ) die ( "USAGE: ./mass_create_items_from_doi_list.php FILE_WITH_DOIs\n") ;

$rows = explode ( "\n" , file_get_contents($file) ) ;
$o = new ORCID ;
foreach ( $rows AS $doi ) {
	$doi = trim ( $doi ) ;
	if ( $doi == '' ) continue ;
	$q = getOrCreateWorkFromIDs ( ['doi'=>$doi] ) ;
	if ( !isset($q) or $q == '' ) continue ;
	addOrCreateAutorsForPaper ( $doi , $q ) ;
	print "$doi\thttps://www.wikidata.org/wiki/{$q}\n" ;
}

?>