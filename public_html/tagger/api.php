<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

//set_time_limit ( 60 * 10 ) ; // 10min
ini_set('memory_limit','500M');

#require_once ( '../php/common.php' ) ;
require_once ( '/data/project/sourcemd/scripts/orcid_shared.php' ) ;

$action = get_request ( 'action' , '' ) ;

$out = [ 'status' => 'OK' , 'data' => [] ] ;

if ( $action == 'get_term_matches' ) {
	$db = openDB ( 'wikidata' , 'wikidata' ) ;
	$lang = get_request ( 'language' , 'en' ) ;
	$terms = json_decode ( get_request ( 'terms' , '[]' ) ) ;

	$lang = $db->real_escape_string ( $lang ) ;
	foreach ( $terms AS $k => $v ) $terms[$k] = $db->real_escape_string ( $v ) ;

	$sql = "
	SELECT term_full_entity_id,term_search_key,
	(SELECT count(DISTINCT pl_from) FROM pagelinks WHERE pl_namespace=0 AND pl_from_namespace=0 AND pl_title=term_full_entity_id ) AS cnt 
	FROM wb_terms 
	WHERE term_entity_type='item' AND term_language='$lang' AND term_type='label' 
	AND term_search_key IN ('" . implode("','",$terms) . "')
	AND NOT EXISTS (SELECT * FROM pagelinks,page WHERE page_namespace=0 AND page_title=term_full_entity_id AND page_id=pl_from AND pl_namespace=0 AND pl_title IN ('Q4167410','Q13406463'))
	" ;
	$sql = trim ( preg_replace ( '/[\n\t]/' , ' ' , $sql ) ) ;
	$out['sql'] = $sql ;
	$result = getSQL ( $db , $sql ) ;
	$tmp = [] ;
	while($o = $result->fetch_object()){
		if ( !isset($tmp[$o->term_search_key]) ) {
			$tmp[$o->term_search_key] = $o ;
		} else {
			if ( $tmp[$o->term_search_key]->cnt < $o->cnt ) $tmp[$o->term_search_key] = $o ;
		}
	}

	foreach ( $tmp AS $o ) {
		$out['data'][] = $o->term_full_entity_id ;
	}

} else if ( $action == 'get_pubmed_abstract' ) {

	$pmid = get_request ( 'pmid' , '' ) ;
	$url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&retmode=xml&rettype=abstract&id={$pmid}" ;
	$xml = @file_get_contents ( $url ) ;
	$out['data'] = '' ;
	if ( preg_match ( '/<AbstractText>(.+?)<\/AbstractText>/' , $xml , $m ) ) $out['data'] = $m[1] ;

} else if ( $action == 'random_paper' ) {

	$db = openDB ( 'wikidata' , 'wikidata' ) ;
	$r = rand()/getrandmax();
	$sql = "
		SELECT * 
		FROM page 
		WHERE page_namespace=0 
		AND EXISTS (SELECT * FROM pagelinks WHERE pl_from=page_id AND pl_namespace=0 AND pl_title='Q13442814' LIMIT 1) 
		AND page_random>=$r
		ORDER BY page_random
		LIMIT 1
	" ;
// 		AND EXISTS (SELECT * FROM pagelinks WHERE pl_from=page_id AND pl_namespace=120 AND pl_title='P50' LIMIT 1) 
// 		AND EXISTS (SELECT * FROM pagelinks WHERE pl_from=page_id AND pl_namespace=120 AND pl_title='P356' LIMIT 1) 

	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()) $out['data'] = $o->page_title ;

}

header('Content-Type: application/json');
print json_encode ( $out ) ;

?>