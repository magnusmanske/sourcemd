// ENFORCE HTTPS
if (location.protocol != 'https:') location.href = 'https:' + window.location.href.substring(window.location.protocol.length);

var current_language = 'en' ;
var api = './api.php' ;
var wd = new WikiData() ;
var widar ;



Vue.component ( 'widar' , {
	data : function () { return { is_logged_in:false , userinfo:{} , widar_api:'/widar/index.php' , loaded:false } } ,
	created : function () {
		widar = this ;
		this.checkLogin()
	} ,
	updated : function () { tt.updateInterface(this.$el) } ,
	mounted : function () { tt.updateInterface(this.$el) } ,
	methods : {
		checkLogin : function () {
			var me = this ;
			$.get ( me.widar_api , {
				action:'get_rights',
				botmode:1
			} , function ( d ) {
				me.loaded = true ;
				if ( typeof d.result.query != 'undefined' && typeof d.result.query.userinfo != 'undefined' ) {
					me.is_logged_in = true ;
					me.userinfo = d.result.query.userinfo ;
				} else {
				}
			} , 'json' ) ;
		} ,
		run : function ( params , callback ) {
			var me = this ;
			params.tool_hashtag = "sourcemd" ;
			params.botmode = 1 ;
			$.get ( me.widar_api , params , function ( d ) {
				if ( d.error != 'OK' ) {
					console.log ( params ) ;
					if ( null != d.error.match(/Invalid token/) || null != d.error.match(/happen/) || null != d.error.match(/Problem creating item/) || ( params.action!='create_redirect' && null != d.error.match(/failed/) ) ) {
						console.log ( "ERROR (re-trying)" , params , d ) ;
						setTimeout ( function () { me.run ( params , callback ) } , 500 ) ; // Again
					} else {
						console.log ( "ERROR (aborting)" , params , d ) ;
						callback ( d ) ; // Continue anyway
					}
				} else {
					callback ( d ) ;
				}
			} , 'json' ) . fail(function() {
				console.log ( "Again" , params ) ;
				me.run ( params , callback ) ;
			}) ;
		} ,
		getUserName : function () { return this.userinfo.name }
	} ,
	template : '#widar-template'
} ) ;



var MainPage = Vue.extend ( {
//	mixins : [itemGroupsMixin] ,
	data : function () { return { } } ,
	created : function () {
		var me = this ;
		me.init() ;
	} ,
	updated : function () { tt.updateInterface(this.$el) ; } ,
	mounted : function () { tt.updateInterface(this.$el) ; } , // $('#catalog_search_query_input').focus();
	methods : {
		init : function () {
			var me = this ;
		}
	} ,
	template: '#main-page-template'
} ) ;

var RandomPage = Vue.extend ( {
//	mixins : [itemGroupsMixin] ,
	data : function () { return { } } ,
	created : function () {
		$.get ( api , { action:'random_paper'} , function ( d ) {
			router.push ( '/paper/' + d.data ) ;
		} , 'json' ) ;
	} ,
//	template: '#main-page-template'
} ) ;

var PaperPage = Vue.extend ( {
//	mixins : [itemGroupsMixin] ,
	props : [ 'paper_q' ] ,
	data : function () { return { i:{} , item_loaded:false , loading:true , tags:{} , tag_order:[] , running:0 , is_tagged:{} , abstract:'' ,
		tag_weight : {
			same_journal : 50 ,
			same_author : 1000 ,
			citations : 2000 ,
			title_keyword : 5000 ,
		}
	} } ,
	created : function () {
		var me = this ;
		me.init() ;
	} ,
	updated : function () { tt.updateInterface(this.$el) ; } ,
	mounted : function () { tt.updateInterface(this.$el) ; } , // $('#catalog_search_query_input').focus();
	methods : {
		setMainTopic : function ( topic_q ) {
			var me = this ;
			var summary = 'Added main topic "' + wd.getItem(topic_q).getLabel(current_language) + '" using [[:toolforge:sourcemd/tagger|SourceMD Tagger]]' ;

			var params = {
				action:'wbeditentity',
				id:me.paper_q,
				data:{
					claims:[]
				}
			} ;

			params.data.claims.push ( {
				mainsnak: {
					snaktype : 'value' ,
					property : 'P921' ,
					datavalue : {
						value : {
							'entity-type' : 'item' ,
							'numeric-id' : topic_q.replace(/\D/g,'') ,
							'id' : topic_q
						} ,
						type : 'wikibase-entityid'
					} ,
					datatype : 'wikibase-item' // Needed?
				} ,
				type : 'statement' ,
				rank : 'normal'
			} ) ;

			params.data = JSON.stringify ( params.data ) ;

			params = {
				action : 'generic' ,
				summary : summary ,
				json : JSON.stringify ( params )
			}

			me.is_tagged[topic_q] = 'tagging' ;
			widar.run ( params , function ( d ) {
				if ( d.error != 'OK' ) {
					alert ( d.error ) ;
					return ;
				}
				me.is_tagged[topic_q] = 'in_item' ;
			} ) ;

		} ,
		init : function () {
			var me = this ;
			me.loading = true ;
			wd.getItemBatch ( [me.paper_q] , function () {
				me.i = wd.getItem ( me.paper_q ) ;
				if ( typeof me.i != 'undefined' ) me.item_loaded = true ;
				me.loading = false ;
				if ( !me.item_loaded ) return ;
				me.running = 1 ;
				me.abstract = '' ;
				me.checkAuthorsForTags() ;
				me.checkJournalForTags() ;
				me.checkTitleForTags() ;
				me.checkCitationsForTags() ;
				me.loadAbstract() ;
			} ) ;
		} ,
		loadAbstract : function () {
			var me = this ;
			var pubmed_id = me.i.getFirstStringForProperty("P698") ;
			if ( pubmed_id == '' ) return ;
			me.running++ ;
			$.get ( api , {
				action:'get_pubmed_abstract',
				pmid:pubmed_id
			} , function ( d ) {
				if ( d.status != 'OK' ) {
					return ;
				}
				me.abstract = d.data ;
			} , 'json' ) 
			.done ( function () {
				me.dataLoaded() ;
			} ) ;
		} ,
		checkTitleForTags : function () {
			var me = this ;
			me.running++ ;

			var keywords = [] ;
			function add2keywords ( part ) {
				keywords.push ( part ) ;
				if ( /-/.test(part) ) keywords.push ( part.replace(/-/g,' ') ) ;
				if ( /\Ss$/.test(part) ) keywords.push ( part.substr(0,part.length-1) ) ;
				if ( /\Ses$/.test(part) ) keywords.push ( part.substr(0,part.length-2) ) ;
			}

			var t = me.i.getLabel ( current_language ) ;
			t = t.replace ( /[\[\]\{\}\(\)_]/g , ' ' ) ;
			t = (' '+t+' ').replace ( /\b(and|or|of|on|a|the|in|to|at|use|using|through|by|not|new|task)\b/gi , "\n") ;
			t = t.replace ( /[\.,;:]/g , "\n") ;
			var parts = t.split ( "\n" ) ;
			$.each ( parts , function ( dummy , part ) {
				part = $.trim ( part.toLowerCase() ) ;
				if ( part == '' ) return ;
				if ( /^[0-9 ]+$/.test(part) ) return ; // Just a number
				if ( part.length <= 2 ) return ; // Too short
				add2keywords ( part ) ;
				var subparts = part.split ( /\s+/ ) ;
				if ( subparts.length == 1 ) return ;
				$.each ( subparts , function ( k , subpart ) {
					add2keywords ( subpart ) ;
				} ) ;
			} ) ;
			
			$.get ( api , {
				action : 'get_term_matches' ,
				language : current_language ,
				terms : JSON.stringify ( keywords )
			} , function ( d ) {
				if ( d.status != 'OK' ) {
					return ;
				}
				$.each ( d.data , function ( dummy , subject ) {
					if ( typeof me.tags[subject] == 'undefined' ) me.tags[subject] = 0 ;
					me.tags[subject] += me.tag_weight.title_keyword ;
				} ) ;
			} , 'json' )
			.done ( function () {
				me.dataLoaded() ;
			} ) ;

		} ,
		checkAuthorsForTags : function () {
			var me = this ;
			var sparql = 'SELECT ?subject { wd:' + me.i.getID() + ' wdt:P50 ?author . ?paper2 wdt:P50 ?author . ?paper2 wdt:P921 ?subject }' ;
			me.running++ ;
			wd.loadSPARQLitems ( sparql , function ( subjects ) {
				$.each ( subjects , function ( dummy , subject ) {
					me.addSubjectPoints ( subject , me.tag_weight.same_author ) ;
				} ) ;
				me.dataLoaded() ;
			} ) ;
		} ,
		checkJournalForTags : function () {
			var me = this ;
			var journals = me.i.getClaimItemsForProperty ( 'P1433' , true ) ;
			if ( journals.length == 0 ) return ;
			var journal_q = journals[0] ;
			var sparql = 'SELECT ?subject { ?paper wdt:P1433 wd:' + journal_q + ' . ?paper wdt:P921 ?subject }' ;
			me.running++ ;
			wd.loadSPARQLitems ( sparql , function ( subjects ) {
				$.each ( subjects , function ( dummy , subject ) {
					me.addSubjectPoints ( subject , me.tag_weight.same_journal ) ;
				} ) ;
				me.dataLoaded() ;
			} ) ;
		} ,
		checkCitationsForTags : function () {
			var me = this ;
			var sparql = 'SELECT ?subject { { wd:' + me.i.getID() + ' wdt:P2860 ?paper . ?paper wdt:P921 ?subject } UNION { ?paper2 wdt:P2860 wd:' + me.i.getID() + ' . ?paper2 wdt:P921 ?subject } }' ;
			me.running++ ;
			wd.loadSPARQLitems ( sparql , function ( subjects ) {
				$.each ( subjects , function ( dummy , subject ) {
					me.addSubjectPoints ( subject , me.tag_weight.citations ) ;
				} ) ;
				me.dataLoaded() ;
			} ) ;
		} ,
		addSubjectPoints : function ( subject , points ) {
			var me = this ;
			if ( typeof me.tags[subject] == 'undefined' ) me.tags[subject] = 0 ;
			me.tags[subject] += points + subject.length ;
		} ,
		dataLoaded : function () {
			var me = this ;
			me.running-- ;
			if ( me.running > 1 ) return ;
			me.tag_order = Object.keys ( me.tags ) ;
			me.tag_order.sort ( function ( a , b ) {
				return me.tags[b] - me.tags[a] ;
			} ) ;

			while ( me.tag_order.length > 100 ) me.tag_order.pop() ;

			var is_tagged = {} ;
			$.each ( me.tags , function ( tag , cnt ) {
				is_tagged[tag] = false ;
				if ( me.i.hasClaimItemLink('P921',tag) ) is_tagged[tag] = 'in_item' ;
			} ) ;
			me.is_tagged = is_tagged ;

			wd.getItemBatch ( me.tag_order , function () {
				me.running = 0 ; // Show results
			} ) ;
		}
	} ,
	template: '#paper-page-template'
} ) ;


const routes = [
  { path: '/', component: MainPage , props:true },
  { path: '/paper/:paper_q', component: PaperPage , props:true },
  { path: '/random', component: RandomPage , props:true },
//  { path: '/group', component: CatalogGroup , props:true },
] ;

var router ;
var app ;
var tt ;


// Load all catalog data first

$(document).ready ( function () {

	var cnt = 1 ;
	function fin () {
		cnt-- ;
		if ( cnt > 0 ) return ;
		router = new VueRouter({routes}) ;
		app = new Vue ( { router } ) .$mount('#app') ;
	}
	
	tt = new ToolTranslation ( {
		tool : 'sourcemd_tagger' ,
		fallback : 'en' ,
		highlight_missing : true ,
		onLanguageChange : function ( new_lang ) {
			current_language = new_lang ;
		} ,
		onUpdateInterface : function () {
		} ,
		callback : function () {
			tt.addILdropdown ( '#tooltranslate_wrapper' ) ;
			fin() ;
		}
	} ) ;
	current_language = tt.language ;
	
	$('#navbar_search_form').submit ( function ( ev ) {
		ev.preventDefault();
		var query = $('#navbar_search_form input[type="text"]').get(0).val() ;
		console.log("1",query);
		return false ;
	} ) ;

} ) ;
