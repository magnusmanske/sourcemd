<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
require_once ( 'php/common.php' ) ;

header('Content-type: application/json');

$callback = $_REQUEST['callback'] ;
$out = [] ;

$db = openToolDB ( 'sourcemd_game_p' ) ;

if ( $_REQUEST['action'] == 'desc' ) {

	$out = [ "label" => [ "en" => "Source MetaData" ] ,
		"description" => [ "en" => "Identify a scientist on a publication. Is it the one suggested?" ] ,
		"icon" => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Mad_scientist.svg/120px-Mad_scientist.svg.png'
	 ] ;

} else if ( $_REQUEST['action'] == 'tiles' ) {

	// GET parameters
	$num = $_REQUEST['num'] * 1 ; // Number of games to return
//	$lang = $_REQUEST['lang'] ; // The language to use, with 'en' as fallback; ignored in this game
	
	$out['tiles'] = [] ;	
	$hadthat = [] ;
	
	$sql = "SELECT count(*) AS cnt FROM candidates WHERE done=0" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$out['left'] = $o->cnt ;
		if ( $out['left'] < 50 ) $out['low'] = 1 ;
	}

	while ( count($out['tiles']) < $num ) {
		$r = rand() / getrandmax() ;
		$sql = "SELECT * FROM candidates WHERE done=0 AND random>=$r ORDER BY random LIMIT $num" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()){
			if ( in_array ( $o->id , $hadthat ) ) continue ;
			$hadthat[] = $o->id ;

			$g = [
				'id' => $o->id ,
				'sections' => [] ,
				'controls' => []
			] ;
		
			$guid = 'Q' . $o->q_publication . '$' . sprintf(
				'%04X%04X-%04X-%04X-%04X-%04X%04X%04X',
				mt_rand( 0, 65535 ),
				mt_rand( 0, 65535 ),
				mt_rand( 0, 65535 ),
				mt_rand( 16384, 20479 ),
				mt_rand( 32768, 49151 ),
				mt_rand( 0, 65535 ),
				mt_rand( 0, 65535 ),
				mt_rand( 0, 65535 )
			);
		
			$qual = '"qualifiers":{"P1545":[{"snaktype":"value","property":"P1545","datavalue":{"value":"'.$o->serialnum.'","type":"string"},"datatype":"string"}]}' ;
			$statement = '{"id":"'.$guid.'","type":"statement","mainsnak":{"snaktype":"value","property":"P50","datatype":"wikibase-item","datavalue":{"value":{"entity-type":"item","numeric-id":'.$o->q_author.'},"type":"wikibase-entityid"}},'.$qual.',"rank":"normal"}' ;
		
			$g['sections'][] = [ 'type' => 'item' , 'q' => 'Q'.$o->q_publication  ] ;
			$g['sections'][] = [ 'type' => 'text' , 'title' => "Publication by {$o->shortname}" , 'url' => $o->url , 'text' => "Author name: {$o->shortname}\nPosition in author list: {$o->serialnum}"  ] ;
			$g['sections'][] = [ 'type' => 'item' , 'q' => 'Q'.$o->q_author  ] ;
			$g['controls'][] = [ 'type' => 'buttons' ,
				'entries' => [ [ 'type' => 'green' , 'decision' => 'yes' , 'label' => 'This is the author' ,
					'api_action' => [ [ 'action'=>'wbsetclaim','claim'=>$statement  ] ,
						[ 'action'=>'wbremoveclaims','claim'=>$o->statement_id  ]
					 ]  ] ,
					[ 'type' => 'white' , 'decision' => 'skip' , 'label' => 'Skip'  ] ,
					[ 'type' => 'blue' , 'decision' => 'no' , 'label' => 'Not the author'  ]
				 ]
			 ] ;
	
			$out['tiles'][] = $g ;
		}
	}
	
} else if ( $_REQUEST['action'] == 'log_action' ) {

	$tile = get_request ( 'tile' , 0 ) * 1 ;
	$decision = get_request ( 'decision' , '' ) ;
	
	$sql = "UPDATE candidates SET done=1 WHERE id=$tile" ;
	$out['sql'][] = $sql ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
	if ( $decision == 'yes' ) {
		$author = '!!' ;
		$sql = "SELECT * FROM candidates WHERE id=$tile" ;
		$out['sql'][] = $sql ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
		while($o = $result->fetch_object()) $author = $o ;
		if ( $author != '' ) {
			$sql = "UPDATE candidates SET done=1 WHERE done=0 AND q_publication={$author->q_publication} AND serialnum={$author->serialnum} AND shortname='" . $db->real_escape_string($author->shortname) . "'" ;
			$out['sql'][] = $sql ;
			if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
		}
	}
	
	
	$out['status'] = 'OK' ;

} else {
	$out['error'] = "No valid action!" ;
}

print $callback . '(' ;
print json_encode ( $out ) ;
print ")\n" ;

?>
