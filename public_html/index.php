<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( '/data/project/sourcemd/scripts/batch_bot.php' ) ;

function showForm () {
	global $thelist , $batch_name ;
	print "<big>
	The IDs and action you enter below will be added to a batch system.<br/>
	It will run in the background, you won't have to keep the browser open.
	</big>
	<p id='login_warning' style='display:none;color:red'>YOU ARE NOT LOGGED INTO <a href='/quickstatements' target='_blank'>QUICKSTATEMENTS</a>!</p>
	<p><span id='welcome'></span> <span id='user_last_batches'></span></p>
	<form method='post'>
	<div style='margin-bottom:20px'>
	Input list
	<textarea style='width:100%' rows='5' name='thelist' placeholder='IDs (one per row), or SPARQL (see checkbox)'>{$thelist}</textarea>
	<input type='hidden' name='username' id='username' type='text' value=''/>
	</div>
	<p><label><input type='checkbox' name='is_sparql' value='1' /> The above is a SPARQL query, rather than a list of IDs</label>; first returned variable will be used as IDs</p>
	<p><button class='btn btn-outline-primary' name='action' value='CREATE_PAPER_BY_ID'>Check/add papers</button> (list: one PMID/DOI/PMCID per row)</p>
	<p><button class='btn btn-outline-primary' name='action' value='ADD_AUTHOR_TO_PUBLICATION'>Add authors to publications</button> by creating new items, and adding to existing authors where possible (list: one Wikidata publication item per row)</p>
	<p><button class='btn btn-outline-primary' name='action' value='ADD_METADATA_FROM_ORCID_TO_AUTHOR'>Add metadata from ORCID authors to Wikidata</button> (list: one Wikidata author item <i>or</i> ORCID ID per row)</p>
	<p><button class='btn btn-outline-primary' name='action' value='EDIT_PAPER_FOR_ORCID_AUTHOR'>Create/amend papers for ORCID authors</button> <i>slow!</i> (list: one Wikidata author item <i>or</i> ORCID ID per row)</p>
	<p><button class='btn btn-outline-primary' name='action' value='CREATE_BOOK_FROM_ISBN'>Create books from ISBN</button> (one ISBN per row; can be 'ISBN|Title' if known, otherwise auto-lookup)</p>
	<p><input type='text' name='batch_name' placeholder='Name of batch (optional)' /></p>
	</form>
	<hr/>
	<form method='get'>
	<p><button class='btn btn-outline-primary' name='action' value='batches'>Show latest batches</button></p>
	</form>
	<div class='alert alert-primary' role='alert'>
		The old SourceMD can be found <a href='index_old.php'>here</a>, the old ORCIDator <a href='orcidator_old.php'>here</a>.
	</div>
	" ;
}

function getStatusClass ( $status ) {
	if ( $status == 'STOPPED' ) return 'badge badge-danger' ;
	if ( $status == 'DONE' ) return 'badge badge-success' ;
	if ( $status == 'TODO' ) return 'badge badge-primary' ;
	return 'badge badge-light' ;
}

$action = get_request ( 'action' , '' )  ;
$thelist = get_request ( 'thelist' , '' )  ;
$batch_name = get_request ( 'batch_name' , '' ) ;

$header = get_common_header ( '' , 'Source MetaData' ) ;
$header = preg_replace('|class="nav-link l_manual" href="//bitbucket.org/magnusmanske">(.+?)</a>|','class="nav-link btn-outline-danger l_manual" href="https://github.com/magnusmanske/papers/issues">Issues</a>',$header);
print $header ;

#print "<div style='display:block !important;font-size:16pt' class='".getStatusClass('STOPPED')."'>The bot for this tool is temporarily deactivated until duplicate item creation can be resolved</div>" ;

?>

<script>
$(document).ready ( function () {
	$.get ( 'https://tools.wmflabs.org/quickstatements/api.php?action=is_logged_in' , function ( d ) {
		if ( d.data.is_logged_in ) {
			$('#username').val(d.data.query.userinfo.name) ;
			$('#welcome').text('Welcome, '+d.data.query.userinfo.name+'!') ;
			$('#user_last_batches').html ( "| <a href='?action=batches&user="+encodeURIComponent(d.data.query.userinfo.name)+"'>Your last batches</a>" ) ;
			
			if ( batch_owner == d.data.query.userinfo.name ) {
				$('#actions_by_batch_owner').show() ;
			}
		} else {
			$("#login_warning").show() ;
		}
	} , 'json' ) ;
} ) ;
</script>

<?PHP

if ( $action == '' ) {
	showForm() ;
	print get_common_footer() ;
	exit(0) ;
}

$bb = new BatchBot () ;

$bot_action = get_request ( 'bot_action' , '' )  ;
if ( $bot_action == 'start' ) {
	$batch_id = get_request ( 'batch' , 0 ) * 1 ;
	$sql = "UPDATE batch SET `status`='TODO' WHERE id={$batch_id}" ;
	$bb->tfc->getSQL ( $bb->dbt , $sql ) ;
}
if ( $bot_action == 'stop' ) {
	$batch_id = get_request ( 'batch' , 0 ) * 1 ;
	$sql = "UPDATE batch SET `status`='STOPPED' WHERE id={$batch_id}" ;
	$bb->tfc->getSQL ( $bb->dbt , $sql ) ;
}


if ( $action == 'batches' ) {
	$batches = [] ;

	$user = get_request ( 'user' , '' ) ;
	$limit = get_request ( 'limit' , 100 ) * 1 ;
	$offset = get_request ( 'offset' , 0 ) * 1 ;

	$sql = "SELECT * FROM batch" ;
	if ( $user != '' ) $sql .= " WHERE `user`='" . $bb->dbt->real_escape_string($user) . "'" ;
	$sql .= " ORDER BY FIELD(status,'TODO','STOPPED','DONE') ASC,last_action DESC LIMIT {$limit} OFFSET {$offset}" ;
	$result = $bb->tfc->getSQL ( $bb->dbt , $sql ) ;
	while($o = $result->fetch_object()) $batches[] = $o ;

#print "<pre>{$sql}</pre>" ;
	print "<table class='table'>" ;
	print "<thead><tr><th>Batch ID</th><th>User</th><th>Batch name</th><th>Batch created</th><th>Last action</th><th>Commands</th><th>Total</th><th>Status</th></tr></thead>" ;
	print "<tbody>" ;
	foreach ( $batches AS $o ) {
		print "<tr>" ;
		print "<td>#<a href='?action=batch&batch={$o->id}'>{$o->id}</a></td>" ;
		print "<td><a href='https://www.wikidata.org/wiki/User:" .urlencode($o->user) ."' target='_blank' class='wikidata'>{$o->user}</a></td>" ;
		print "<td>{$o->name}</td>" ;
		print "<td>{$o->date_created}</td>" ;
		print "<td>{$o->last_action}</td>" ;

		$j = json_decode ( $o->overview ) ;
		print "<td>" ;
		foreach ( $j AS $k => $v ) {
			if ( $k == 'TOTAL' ) continue ;
			print "{$k}:{$v}&nbsp;" ;
		}
		print "</td>" ;
		print "<td>{$j->TOTAL}</td>" ;

		print "<td><span class='".getStatusClass($o->status)."'>{$o->status}</span></td>" ;
		print "</tr>" ;
	}
	print "</tbody></table>" ;

	print "<a href='?action=batches&limit={$limit}&offset=".($offset+$limit)."'>Next page</a> | Show" ;
	print " <a href='?action=batches&limit=100&offset={$offset}'>100</a> |" ;
	print " <a href='?action=batches&limit=250&offset={$offset}'>250</a> |" ;
	print " <a href='?action=batches&limit=500&offset={$offset}'>500</a> |" ;
	print " <a href='?action=batches&limit=1000&offset={$offset}'>1000</a> batches" ;

	print get_common_footer() ;
	exit(0) ;
}

if ( $action == 'batch' ) {
	
	$batch_id = get_request ( 'batch' , 0 ) * 1 ;
	$bi = $bb->getBatchInfo ( $batch_id ) ;
	$j = json_decode ( $bi->overview ) ;

	print "<script>var batch_owner='{$bi->user}';</script>" ;

	print "<h2>Batch #{$batch_id}</h2>" ;
	print "<table class='table'><tbody>" ;
	if ( $bi->name != '' ) print "<tr><th>Name</th><td>" . htmlspecialchars($bi->name) . "</td></tr>" ;	
	print "<tr><th>User</th><td style='width:100%'><a href='https://www.wikidata.org/wiki/User:" .urlencode($bi->user) ."' target='_blank' class='wikidata'>{$bi->user}</a>" ;
	print " (<a href='?action=batches&user=" .urlencode($bi->user) ."'>Batches by this user</a>)" ;
	print "</td></tr>" ;	
	print "<tr><th>Status</th><td>{$bi->status}</td></tr>" ;	
	print "<tr><th>Actions</th><td>" ;
	if ( $bi->qs_temporary_batch_id != '' ) print "<a class='btn btn-outline-primary' target='_blank' href='https://tools.wmflabs.org/editgroups/b/QSv2T/".urlencode(preg_replace('/\D/','',$bi->qs_temporary_batch_id))."'>Discuss/revert</a> " ;
	print "<span id='actions_by_batch_owner' style='display:none'>" ;
	if ( $bi->status == 'TODO' ) print "<a class='btn btn-outline-danger' href='?action=batch&batch={$batch_id}&bot_action=stop'>Stop batch</a> " ;
	if ( $bi->status == 'STOPPED' ) print "<a class='btn btn-outline-success' href='?action=batch&batch={$batch_id}&bot_action=start'>Restart batch</a> " ;
	print "</span>" ;
	print "</td></tr>" ;
	print "</tbody></table>" ;

	print "<h3>Command statistics</h3>" ;
	print "<table class='table'>" ;
	print "<tbody>" ;
	foreach ( $j AS $k => $v ) {
		if ( $k == 'TOTAL' ) continue ;
		print "<tr><th>{$k}</th><td style='width:100%'>{$v}</td></tr>" ;
	}
	print "</tbody><tfoot><tr><th>TOTAL</th><td>{$j->TOTAL}</td></tr>" ;
	print "</tfoot></table>" ;

	if ( isset($j->FAILED) and $j->FAILED > 0 ) {
		print "<h3>Failed</h3>" ;
		print "<table class='table'>" ;
		print "<tbody>" ;
		$sql = "SELECT * FROM `command` WHERE batch_id={$batch_id} AND `status` IN ('FAILED')" ;
		$result = $bb->tfc->getSQL ( $bb->dbt , $sql ) ;
		while($o = $result->fetch_object()) {
			print "<tr>" ;
			print "<td>{$o->mode}</td>" ;
			print "<td>{$o->identifier}</td>" ;
			print "<td>{$o->note}</td>" ;
			if ( $o->q != '' ) print "<td><a href='https://www.wikidata.org/wiki/{$o->q}' target='_blank' class='wikidata'>{$o->q}</a></td>" ;
			print "</tr>" ;
		}
		print "</tbody></table>" ;
	}

	print "<p><i>Note:</i> Depending on context, 'FAILED' can just mean 'an item already exists'.</p>" ;

	print get_common_footer() ;
	exit(0) ;
}

function isValidAuthorID ( $id ) {
	if ( preg_match ( '/^Q\d+$/' , $id ) ) return true ; # Qxxx
	if ( preg_match ( '/^\d{4}-\d{4}-\d{4}-\S{4}$/' , $id ) ) return true ; # ORCID
	return false ; # Nope
}

$ids = [] ;
$is_sparql = get_request ( 'is_sparql' , 0 ) ;
if ( $is_sparql ) {
	$j = $bb->tfc->getSPARQL ( $thelist ) ;
	$varname = $j->head->vars[0] ;
	foreach ( $j->results->bindings AS $v ) {
		$v = $v->$varname ;
		$id = '' ;
		if ( $v->type == 'uri' and preg_match ( '|http://www.wikidata.org/entity/(Q\d+)|' , $v->value , $m ) ) $id = $m[1] ;
		else $id = trim ( $v->value ) ;
		if ( $id != '' ) $ids[] = $id ;
	}
} else {
	$rows = explode("\n",$thelist) ;
	foreach ( $rows AS $id ) {
		$id = trim($id) ;
		if ( $id == '' ) continue ;
		if ( $action == 'ADD_AUTHOR_TO_PUBLICATION' and !preg_match ( '/^Q\d+$/' , $id ) ) continue ;
		if ( $action == 'ADD_METADATA_FROM_ORCID_TO_AUTHOR' and !isValidAuthorID($id) ) continue ;
		if ( $action == 'EDIT_PAPER_FOR_ORCID_AUTHOR' and !isValidAuthorID($id) ) continue ;
		$ids[] = $id ;
	}
}

$ids = array_unique ( $ids ) ;

if ( count($ids) == 0 ) {
	print '<div class="alert alert-danger" role="alert">Empty or invalid IDs in list</div>' ;
	showForm() ;
	print get_common_footer() ;
	exit(0) ;
}

$user = get_request ( 'username' , '' ) ;
if ( $user == '' ) {
	print "<p>Not logged in!</p>" ;
	showForm() ;
	print get_common_footer() ;
	exit(0) ;
}

$batch_name = get_request ( 'batch_name' , '' ) ;

$batch_id = $bb->createNewBatch ( $user , $batch_name ) ;
foreach ( $ids AS $id ) {
	$bb->addNewCommandToBatch ( $batch_id , $action , $id ) ;
}

print "<p>Your new batch is <a href='?action=batch&batch={$batch_id}'>#{$batch_id}</a></p>" ;
print get_common_footer() ;

?>