<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
set_time_limit ( 60 * 60 ) ; // 1h

require_once ( 'php/common.php' ) ;
require_once ( 'php/wikidata.php' ) ;
require_once ( '../sourcemd.php' ) ;

$title = 'Fix Source MetaData items' ;
$ids = get_request ( 'ids' , '' ) ;
$these_props_only = trim ( strtoupper ( get_request ( 'these_props_only' , '' ) ) ) ;

if ( isset($_REQUEST['doit'] ) ) {

	print get_common_header ( '' , $title ) ;
	print "<ol>" ;
	
	$all = array() ;
	$ids = explode ( "\n" , $ids ) ;
	foreach ( $ids AS $id ) {
		$id = strtoupper ( trim ( $id ) ) ;
		if ( !preg_match ( '/^Q\d+$/' , $id ) ) continue ;
		print "<li><a href='https://www.wikidata.org/wiki/$id' target='_blank'>$id</a>: " ;
		$smd = new SourceMD ( $id ) ;
		if ( $these_props_only != '' ) {
			$smd->these_props_only = explode ( "\n" , $these_props_only ) ;
		}
		if ( $smd->number_of_existing_items != 1 ) {
			print "Found " . $smd->number_of_existing_items . " items, skipping" ;
		} else {
			$rows = $smd->generateQuickStatements() ;
			if ( count($rows) == 0 ) {
				print "No new statements found" ;
			} else {
				print count($rows) . " new statement" . (count($rows)>1?'s':'') ;
				foreach ( $rows AS $row ) $all[] = $row ;
			}
//			print "<pre>" ; print_r ( $rows ) ; print "</pre>" ;
		}
		print "</li>" ;
		myflush() ;
	}
	
	print "</ol>" ;
	
	print "<form class='form from-inline' action='/wikidata-todo/quick_statements.php' method='post'>" ;
	print "<textarea name='list' rows=15 style='width:100%'>" . implode ( "\n" , $all ) . "</textarea>" ;
	print "<input type='submit' class='btn btn-primary' name='doit' value='Open in QuickStatements' /></form>" ;
	
	print get_common_footer() ;
	
	exit(0) ;
}

print get_common_header ( '' , $title ) ;

print "<form method='get' class='form form-inline'>
Wikidata items to update (one per row, 'Qxxx'):
<textarea name='ids' style='width:100%' rows=10>$ids</textarea>
<div style='margin-top:10px;'>Generate only statements for these properties (one per row; optional)</div>
<textarea name='these_props_only' rows=2 style='width:100%'>$these_props_only</textarea>
<input type='submit' class='btn btn-primary' name='doit' value='Check source' />
</form>" ;

print get_common_footer() ;

?>