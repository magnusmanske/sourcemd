<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;
require_once ( 'php/wikidata.php' ) ;
require_once ( '../sourcemd.php' ) ;

$ids = explode ( "\n" , get_request ( 'id' , '' ) ) ;
/*
$toolname = 'SourceMD' ;

function getQS () {
	global $toolname ;
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/mix-n-match/reinheitsgebot.conf" ;
	$qs->toolname = $toolname ;
	$qs->sleep = 1 ;
	$qs->use_command_compression = true ;
	return $qs ;
}

// $commands = [] , one QS V1 command per element, no newlines
function runCommandsQS ( &$qs , $commands ) {
	if ( count($commands) == 0 ) return ;
	$commands = implode ( "\n" , $commands ) ;
	$tmp_uncompressed = $qs->importData ( $commands , 'v1' ) ;
	$tmp['data']['commands'] = $qs->compressCommands ( $tmp_uncompressed['data']['commands'] ) ;
	$qs->runCommandArray ( $tmp['data']['commands'] ) ;
	if ( !isset($qs->last_item) ) return ;
	$last_item = 'Q' . preg_replace ( '/\D/' , '' , "{$qs->last_item}" ) ;
	return $last_item ;
}
*/

// http://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/?tool=my_tool&email=my_email@example.com&ids=10.1093/nar/gks1195

if ( isset($_REQUEST['doit'] ) ) {

	$rows = [] ;
	print get_common_header ( '' , 'Source MetaData' ) ;
	foreach ( $ids AS $id ) {
		if ( $id == '' ) continue ;
		$smd = new SourceMD ( $id ) ;
		$rows_sub = $smd->generateQuickStatements() ;
		foreach ( $rows_sub AS $r ) $rows[] = $r ;
#		$qs = getQS () ;
#		runCommandsQS ( $qs , $rows ) ;
	}
	print "<p>You can create a new item, or update an existing one, for this source in QuickStatements:</p>" ;
/*
	print "<form class='form from-inline' action='https://tools.wmflabs.org/wikidata-todo/quick_statements.php' method='post'><textarea name='list' rows=15 style='width:100%'>" ;
	print implode ( "\n" , $rows ) ;
	print "</textarea><input type='submit' class='btn btn-primary' name='doit' value='Open in QuickStatements' /></form>" ;
*/
print "
<form action='//tools.wmflabs.org/quickstatements/api.php' method='post' target='_blank'>
<input type='hidden' name='action' value='import' />
<input type='hidden' name='format' value='v1' />
<input type='hidden' name='temporary' value='1' />
<input type='hidden' name='openpage' value='1' />
<textarea name='data' rows=15 style='width:100%'>
" . implode ( "\n" , $rows ) . "
</textarea>
<button class='btn btn-outline-primary' name='yup'>Open in QuickStatements</button>
</form>
" ;


	print get_common_footer() ;
	
	exit(0) ;
}

print get_common_header ( '' , 'Source MetaData' ) ;

print "<form method='get' class='form form-inline'>
Source ID:
<textarea name='id' rows='5' style='width:100%' placeholder='PMID/DOI/PMCID, one per row'>" . implode ( "\n" , $ids ) . "</textarea>
<input type='submit' class='btn btn-primary' name='doit' value='Check source' />
</form>" ;

print get_common_footer() ;

?>