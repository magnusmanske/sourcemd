<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

//set_time_limit ( 60 * 10 ) ; // 10min
ini_set('memory_limit','500M');

require_once ( '/data/project/sourcemd/scripts/orcid_shared.php' ) ;

$tfc = new ToolforgeCommon ;
$use_oauth = false ; // DOES NOT WORK

function getOauthStatus() {
	global $use_oauth ;
#	$tmp = new QuickStatements ;
#	$oa = $tmp->getOA() ;
	$oa = new MW_OAuth ( ['tool'=>'quickstatements','language'=>'wikidata','project'=>'wikidata','apiUrl'=>'https://www.wikidata.org/w/api.php','ini_file'=>'/data/project/sourcemd/oauth.ini'] ) ;
	$use_oauth = $oa->isAuthOK() ;
}

function qlink ( $q ) {
	global $wil ;
	$i = $wil->getItem ( $q ) ;
	$label = $q ;
	if ( isset($i) ) $label = $i->getLabel() ;
	if ( $label == $q ) return "<a href='https://www.wikidata.org/wiki/$q' target='_blank'>$q</a>" ;
	return "<a href='https://www.wikidata.org/wiki/$q' target='_blank'>$label</a> <small>[$q]</small>" ;
}

function getQS () {
	global $use_oauth ;
	$toolname = '' ; // Or fill this in manually
	$path = realpath(dirname(__FILE__)) ;
	$user = get_current_user() ;
	if ( $toolname != '' ) {}
	else if ( preg_match ( '/^tools\.(.+)$/' , $user , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/data\/project\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	else if ( preg_match ( '/^\/mnt\/nfs\/[^\/]+\/([^\/]+)/' , $path , $m ) ) $toolname = $m[1] ;
	if ( $toolname == '' ) die ( "getQS(): Can't determine the toolname for $path\n" ) ;
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ; //$use_oauth ;
	$qs->bot_config_file = "/data/project/$toolname/bot.ini" ;
	$qs->toolname = 'SourceMD:ORCIDator' ;
//	$qs->sleep = 1 ;
	return $qs ;
}

function show_form () {
	global $thelist ;
	print "<form method='post'>
	<div style='margin-bottom:20px'>
	Input list
	<textarea style='width:100%' rows='5' name='thelist'>{$thelist}</textarea>
	</div>
	<p><button class='btn btn-primary-outline' name='action' value='scan_papers'>Add authors to publications</button> by creating new items, and adding to existing authors where possible (list: one Wikidata publication item per row)</p>
	<p><button class='btn btn-primary-outline' name='action' value='authors_meta'>Add metadata from ORCID authors to Wikidata</button> (list: one Wikidata author item per row)</p>
	<p><button class='btn btn-primary-outline' name='action' value='works4authors'>Create/amend papers for ORCID authors</button> <i>slow!</i> (list: one Wikidata author item per row)</p>
	</form>" ;
}


getOauthStatus() ;

//	print "<pre>" ; print_r ( $ret ) ; print "</pre>" ;
print get_common_header('','ORCIDator') ;

//if ( $use_oauth ) print "<p>Using OAuth</p>" ; else print "<p>Not using OAuth</p>" ;

$thelist = get_request ( 'thelist' , '' ) ;
$action = get_request ( 'action' , '' ) ;
$wil = new WikidataItemList () ;


if ( $action == 'scan_papers' ) {

	$create_new_authors = true ;
	initAuthorsWithORCID () ;
	$list = explode ( "\n" , $thelist ) ;
	$q2doi = qs2dois ( $list ) ;
	$wil->loadItems ( array_keys ( $q2doi ) ) ;

	foreach ( $q2doi AS $q => $doi ) {
		$tfc->flush() ;
		print "<li>" . qlink($q) . ": " ;
		$matches = addOrCreateAutorsForPaper ( $doi , $q ) ;
		$wil->loadItems ( array_keys ( $matches ) ) ; // Should already be loaded, just making sure...
		$commands = '' ;
		foreach ( $matches AS $q => $orcid ) {
			$i = $wil->getItem ( $q ) ;
			if ( $i->hasClaims ( 'P496' ) ) {
	//			logit ( "$q already has ORCID" ) ;
				continue ;
			}
			if ( isset ( $set_cache[$q] ) ) {
				continue ;
			}
			$set_cache[$q] = $orcid ;
			$commands .= "$q\tP496\t\"$orcid\"\n" ;
		}
		if ( $commands == '' ) {
		} else {
			$qs = getQS() ;
			$tmp = $qs->importData ( $commands , 'v1' ) ;
			$qs->runCommandArray ( $tmp['data']['commands'] ) ;
		}
		print "done.</li>" ;
	}

	
} else if ( $action == 'authors_meta' ) {

	$list = explode ( "\n" , $thelist ) ;
	$wil->loadItems ( $list ) ;
	print "<ol>" ;
	foreach ( $list AS $q ) {
		$tfc->flush() ;
		$q = trim ( strtoupper ( $q ) ) ;
		if ( $q == '' ) continue ;
		if ( !preg_match ( '/^Q\d+$/' , $q ) ) {
			print "<li>'$q' is not a Wikidata item</li>\n" ;
			continue ;
		}
		print "<li>" . qlink($q) . ": " ;
		$res = add_person_ids_from_orcid ( $q ) ;
		if ( $res == 'OK' ) print "nothing to do" ;
		else print $res ;
		print "</li>\n" ;
	}
	print "</ol>" ;

} else if ( $action == 'works4authors' ) {

	$o = new ORCID ;
	$list = explode ( "\n" , $thelist ) ;
	$wil->loadItems ( $list ) ;
	print "<ol>" ;
	foreach ( $list AS $q ) {
		$i = $wil->getItem ( $q ) ;
		if ( !isset($i) ) continue ;
		$orcid = $i->getFirstString ( 'P496' ) ;
		if ( !$o->isValidORCID($orcid) ) continue ;
		$q = $i->getQ() ;
		print "<li>" . qlink($q) . ": " ;

		print "<ol>" ;
		$works = $o->getWorksForORCID ( $orcid ) ;

		# Get IDs for works bu this author (P50); no need to check those
		$sparql = "SELECT ?doi ?pmid ?pmc { ?work wdt:P50 wd:Q27664356 OPTIONAL { ?work wdt:P356 ?doi } OPTIONAL { ?work wdt:P698 ?pmid } OPTIONAL { ?work wdt:P932 ?pmc } }" ;
		$j = $tfc->getSPARQL ( $sparql ) ;
		$ids = [ 'doi'=>[] , 'pmc'=>[] , 'pmid'=>[] ] ;
		foreach ( $j->results->bindings AS $v ) {
			if ( isset($v->doi) ) $ids['doi'][strtoupper($v->doi->value)]=1 ;
			if ( isset($v->pmid) ) $ids['pmid'][$v->pmid->value] = 1 ;
			if ( isset($v->pmc) ) $ids['pmc'][preg_replace('/^\D+/','',$v->pmc->value)] = 1 ;
		}
		foreach ( $works AS $w ) {
			if ( isset($w['doi']) and isset($ids['doi'][strtoupper($w['doi'])]) ) continue ; // Already on WD
			if ( isset($w['pmid']) and isset($ids['pmid'][$w['pmid']]) ) continue ; // Already on WD
			if ( isset($w['pmc']) and isset($ids['pmc'][preg_replace('/^\D+/','',$w['pmc'])]) ) continue ; // Already on WD
#			print "<pre>" ; print_r ( $w ) ; print "</pre>" ;
			$action_taken = '' ;
			$work_q = getOrCreateWorkFromIDs ( $w ) ;
			if ( !isset($work_q) ) {
				print "<li>Can't create new item for work " . json_encode($w) . "</li>\n" ; $tfc->flush() ;
				continue ;
			}
			if ( $action_taken == '' ) continue ;

			$wil->loadItems ( [ $work_q ] ) ;
			if ( $action_taken == 'CREATE' ) print "<li>New item: " . qlink($work_q) . "</li>" ;
			else if ( $action_taken == 'EDITED' ) print "<li>Edited item: " . qlink($work_q) . "</li>" ;
			else print "<li>Amended item: " . qlink($work_q) . "</li>" ;
			$tfc->flush() ;
		}
		print "</ol>" ;
		$tfc->flush() ;

#		$res = add_person_ids_from_orcid ( $q ) ;
#		if ( $res == 'OK' ) print "nothing to do" ;
#		else print $res ;
		print "</li>\n" ;
		$tfc->flush() ;
	}
	print "</ol>" ;


} else if ( $action != '' )  {
	print "<b>UNKNOWN ACTION '$action'</b>" ;
}

if ( $action != '' ) print "<hr/>" ;

show_form() ;

?>